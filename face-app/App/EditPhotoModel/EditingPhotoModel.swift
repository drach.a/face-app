//
//  EditingPhotoModel.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 13.08.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import UIKit

/// Indicates the editing photo model..
struct EditingPhotoModel {
    
    // - MARK: Structs
    /// The photo editing state.
    enum State {
        case edited
        case original
    }
    
    // - MARK: Properties
    
    /// Photo state.
    var state: State = .edited
    /// Contains original image.
    var originalImage = UIImage()
    
    var subOriginalImage = UIImage()
    
    /// Containes edited image.
    var editedImage =  UIImage()
    /// Indicates current  FaceCropper instance.
    var faceCropper: FaceCropper?
    /// Indicates an array of croped faces.
    var croppedFaces: [FaceCropper] = []
    
    
    // - MARK: Effects values
    var sepia: Float = 0
    var bloom: Float = 0
    var chrome: Float = 0
    var vibrance: Float = 0
    var hue: Float = 0
    var gamma: Float = 1
    var exposure: Float = 0
    var zoomBlur: Float = 0
    var scale: Float = 1
    var flight: Float = 0
    
    // - MARK: Lighting values
    var brightness: Float = 0
    var contrast: Float = 1
    var highlights: Float = 0
    var shadows: Float = 0
    
    // - MARK: Color values
    var saturation: Float = 1
    var warmth: Float = 0
    var fade: Float = 0
    var tint: Float = 0.0
    
    // - MARK: Actions
    
    /// Changes states.
    mutating func changeState() {
        if state == .original {
            state = .edited
        }
        else {
            state = .original
        }
    }
    
    /// Mirrors croped face to it's oposite side.
    func mirrorFace() {
        guard let image = faceCropper?.shapeMask.image else { return }
        faceCropper?.shapeMask.image = mirror(image)
    }
    
    /// Sets original image to edited.
    mutating func setOriginal () {
        editedImage = subOriginalImage
    }
    /// Sets effects values to default.
    mutating func setDefaultEffects() {
        sepia = 0
        bloom = 0
        chrome = 0
        vibrance = 0
        hue = 0
        gamma = 1
        exposure = 0
        zoomBlur = 0
        scale = 1
        flight = 0
        brightness = 0
        contrast = 1
        highlights = 0
        shadows = 0
        saturation = 1
        warmth = 0
        fade = 0
        tint = 0
    }
    
    
    /// Mirrors UIImage
    /// - Parameters:
    ///   - image: Indicates UiImage to be mirrored.
    /// - Returns: - Mirrored UIImage.
    private func mirror(_ image: UIImage) -> UIImage {
        var mirror = UIImage.Orientation.upMirrored
        
        switch image.imageOrientation {
        case .down:
            mirror = .downMirrored
        case .up:
            mirror = .upMirrored
        case .left:
            mirror = .leftMirrored
        case .right:
            mirror = .rightMirrored
        case .upMirrored:
            mirror = .up
        case .downMirrored:
            mirror = .down
        case .leftMirrored:
            mirror = .left
        case .rightMirrored:
            mirror = .right
        default:
            break
        }
        guard let cgImage = image.cgImage else { return image }
        
        return UIImage(cgImage: cgImage, scale: image.scale, orientation: mirror)
    }
    
    
}
