//
//  StartNavigationBar.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 13.08.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import UIKit

/// Indicates a StartNavigationBar for a ViewController.
class StartNavigationBar: UIView {
    
    // - MARK: Properties
    
    // - MARK: Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        loadFromNib(name: "StartNavigationBar")
    }
  
    // - MARK: Private IBActions
    
    @IBAction private func redoAction(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name("redo"), object: nil)
    }
    
    @IBAction private func undoAction(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name("undo"), object: nil)
    }
    
    @IBAction private func openPhotoEditorAction(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name("edit"), object: nil)
    }
    
    @IBAction private func showShapesAction(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name("showShapes"), object: nil)
    }
    
    @IBAction private func savePhoto(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name("save"), object: nil)
    }
    
    
    @IBAction private func mirrorAction(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name("mirror"), object: nil)
    }
    
    
}
