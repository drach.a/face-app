//
//  StartTabBar.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 13.08.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import UIKit

/// Indicates custom TabBarView.
class StartTabBar: UIView {
    
    // - MARK: Properties

    @IBOutlet weak var addPhoto: UIView!
    @IBOutlet weak var clearButtonOutlet: UIButton!
    @IBOutlet weak var swapButtonOutlet: UIButton!
    @IBOutlet weak var pasteButtonOutlet: UIButton!
    @IBOutlet weak var copyButtonOutlet: UIButton!
    
    // - MARK: Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setProperties()
    }
    
    // - MARK: Private Actions
    
    /// Initializes the StartTabBar from a xib file called - "StartTabBar".
    private func setProperties() {
        self.loadFromNib(name: "StartTabBar")
        addPhoto.layer.cornerRadius = 36
    }
    
    // MARK: - Private IBActions
    
    @IBAction private func selectPhoto(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name("selectPhoto"), object: nil)
    }
    
    @IBAction private func clearButtonAction(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name("clear"), object: nil)
    }
    
    @IBAction private func swapButtonAction(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name("swap"), object: nil)
    }
    
    @IBAction private func copyButtonAction(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name("crop"), object: nil)
    }
    
    @IBAction private func pasteButtonAction(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name("paste"), object: nil)
    }
    
    
}
