//
//  StartViewController+Extensions.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 13.08.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import UIKit


// - MARK: UIImagePickerControllerDelegate & UINavigationControllerDelegate

extension StartViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.originalImage] as? UIImage {
            
            let resizedImage = image.resized(with: 3500.0)
            photoModel = EditingPhotoModel()
            photoModel.originalImage = resizedImage
            photoModel.editedImage = resizedImage
            selectedImage.image = resizedImage
        }
        dismiss(animated: true)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true)
    }
    
}

// - MARK: ShapesCollectionDelegate

extension StartViewController: ShapesCollectionDelegate {
    func shapesCollection(_ shapesCollection: ShapesCollection, didPickShape shape: String) {
        add(with: shape)
    }
    
}

// - MARK: EditorControllerDelegate

extension StartViewController: EditorControllerDelegate {
    
    func editorController(_ editorController: PhotoEditorViewController, didFinishEditing finished: Bool) {
        guard let image = editorController.photoModel?.editedImage else { return }
        photoModel.editedImage = image
        selectedImage.image = photoModel.editedImage
        photoModel.croppedFaces = []
        photoModel.state = .edited
        momentoOperator.clean()
        for face in selectedImage.subviews {
            face.removeFromSuperview()
        }
    }
    
    func editorController(_ editorController: PhotoEditorViewController, didLoadView: Bool) {
        editorController.photoModel = photoModel
    }
    
}
