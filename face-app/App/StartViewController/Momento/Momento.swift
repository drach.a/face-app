//
//  Momento.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 11.08.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import UIKit

/// Indicates a container for saving an object state.
struct Momento {
    
    // - MARK: Properties
    
    /// Photo state.
    var state: EditingPhotoModel.State
    /// Indicates cropped faces..
    var faces: [FaceCropper]
    
}

