//
//  MomentoOperator.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 13.08.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import UIKit

/// Operates the Momentoes objects.
class MomentoOperator: NSObject {
    
    /// Indicates the operator state.
    enum State {
        case redo
        case undo
        case none
    }
    
    // - MARK: Properties
    private var state: State = .none
    
    // Indicates momento operator container count.
    var isEmpty: Bool {
        momentoes.isEmpty ? true : false
    }
    
    /// Indicates current index.
    private var currentIndex = 0
    
    /// Indicates array of momentoes
    private var momentoes: [Momento] = []
    
    
    // - MARK: Actions
    
    /// Must be called after each operation where I want to save samething.
    func read(from model: EditingPhotoModel) {
        let momento = Momento(state: model.state, faces: model.croppedFaces)
        momentoes.insert(momento, at: momentoes.startIndex)
    }
    
    /// Reads saved states from momento.
    func write(to model: inout EditingPhotoModel, with state: State) {
        switch state {
        case .redo:
            if currentIndex < momentoes.count {
            currentIndex += 1
            }
            else { break}
        case .undo:
            if currentIndex != 0 {
            currentIndex -= 1
            }
            else { break }
        case .none:
            return
        }
        if momentoes.indices.contains(currentIndex) {
            model.croppedFaces = momentoes[currentIndex].faces
            model.state = momentoes[currentIndex].state
        }
        else {
            model.croppedFaces = []
            model.state = .edited
        }
       // print(currentIndex)
    }
    
    
    /// Cleans momentoes from the operator.
    func clean() {
        momentoes = []
        currentIndex = 0
        state = .none
    }
    
}
