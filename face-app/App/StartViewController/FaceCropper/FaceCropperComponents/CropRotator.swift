//
//  CropRotator.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 11.08.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import UIKit


/// Rotates UIView in degrees.
class CropRotator: UIImageView {

    // - MARK: Properties
    
    /// Indicates the rotation state.
    var canRotate = false
    /// Touch in the view.
    var innerTouch: CGPoint?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setProperties()
        // Stop the spinner when the screen is tapped
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    convenience init() {
        self.init(frame: .zero)
    }
    
    // - MARK: Actions
    
    /// Adds to super view.
    func add(to view: UIView, with size: CGFloat) {
        frame = CGRect(x: frame.width - size, y: size, width: size, height: size)
        view.addSubview(self)
        setConstraints(in: view, with: size)
    }
    
    // - MARK: Private Actions
    
    /// Sets properties.
    private func setProperties() {
        backgroundColor = .clear
        isUserInteractionEnabled = true
        isOpaque = true
        translatesAutoresizingMaskIntoConstraints = false
        contentMode = .scaleAspectFit
        image = UIImage(named: "rotateFace")
    }
    
    /// sets constraints
    private func setConstraints(in view: UIView, with constant: CGFloat) {
        bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        widthAnchor.constraint(equalToConstant: constant).isActive = true
        heightAnchor.constraint(equalToConstant: constant).isActive = true
    }
    
}



