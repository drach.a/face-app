//
//  CropMover.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 09.08.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import UIKit

/// Indicates the FaceCropper mover.
class CropMover: UIImageView {
    
    // - MARK: Properties
    
    /// Indicates the device state.
    private let device = UIDevice.current.userInterfaceIdiom
    /// Indicates the CropMover size.
    private var size: CGSize {
        return device == .pad ? CGSize(width: 40, height: 40) : CGSize(width: 25, height: 25)
    }
    
    // - MARK: Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setProperties()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    convenience init() {
            self.init(frame: .zero)
        }
    
    // - MARK: Actions
    
    /// Adds the mover to superView.
    /// - Parameters:
    ///  - superView: - The CropMover superView.
    func add(to superView: UIView) {
        frame = CGRect(x: superView.center.x, y: superView.center.y, width: size.width, height: size.height)
        superView.insertSubview(self, at: superView.subviews.endIndex)
        setConstraints(in: superView)
    }

    
    // - MARK: Private Actions
    
    /// Sets the CropMover properties.
    private func setProperties() {
        backgroundColor = .clear
        isUserInteractionEnabled = true
        isOpaque = true
        translatesAutoresizingMaskIntoConstraints = false
        contentMode = .scaleAspectFit
        image = UIImage(named: "cropMover")
    }
    
    /// Sets the mover constraints.
    /// - Parameters:
    ///  - view: - The CropMover superView.
    private func setConstraints(in view: UIView) {
        centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        heightAnchor.constraint(equalToConstant: size.height).isActive = true
        widthAnchor.constraint(equalToConstant: size.width).isActive = true
    }
    
}
