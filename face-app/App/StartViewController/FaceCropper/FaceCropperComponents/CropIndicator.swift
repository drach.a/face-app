//
//  CropIndicator.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 09.08.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import UIKit

/// Indicates the FaceCropper frame size while moving, rotating and resizing.
class CropIndicator: UIImageView {
    
    // - MARK: Structs
    
    /// Indicates the CropIndicator frame side in superview.
    enum Side {
        case top
        case bottom
        case right
        case left
        case none
    }
    
    // - MARK: Properties
    
    /// Indicates superview frame side, by default is none.
    var frameSide: Side = .none
    
    // - MARK: Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setProperties()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    convenience init() {
        self.init(frame: .zero)
    }
    
    // - MARK: Actions
    
    /// Adds the CropIndicator to it's superView.
    /// - Parameters:
    ///   - superView: - Indicates the CropIndicator superview as  UIView.
    ///   - ratioSize: - Indicates the CropIndicator ratio size width & height.
    func add(to superView: UIView, pinTo side: Side, with ratioSize: CGFloat) {
        frameSide = side
        frame = CGRect(x: 0, y: 0, width: ratioSize, height: ratioSize)
        superView.insertSubview(self, at: superView.subviews.endIndex)
        setConstraints(to: superView, with: ratioSize)
    }
    
    // - MARK: Private Actions
    
    /// Sets constraints depending on superview frame.
    /// - Parameters:
    ///   - view: - Indicates the CropIndicator superview frame side.
    ///   - ratioSize: - Indicates the CropIndicator ratio size width & height set in constraints.
    private func setConstraints(to view: UIView, with ratioSize: CGFloat) {
        switch frameSide {
        case .top:
            topAnchor.constraint(equalTo: view.topAnchor, constant: -ratioSize).isActive = true
            centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        case .bottom:
            bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: ratioSize).isActive = true
            centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        case .left:
            leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: -ratioSize).isActive = true
            centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        case .right:
            trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: ratioSize).isActive = true
            centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        case .none:
            return
        }
        widthAnchor.constraint(equalToConstant: ratioSize).isActive = true
        heightAnchor.constraint(equalToConstant: ratioSize).isActive = true
    }
    

    /// Sets the FaceCropper properties.
    private func setProperties() {
        image = UIImage(named: "cropResizer")
        contentMode = .scaleAspectFit
        backgroundColor = .clear
        isUserInteractionEnabled = true
        isOpaque = true
        translatesAutoresizingMaskIntoConstraints = false
    }
    
    
}
