//
//  FaceCropper.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 09.08.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import UIKit

/// Indicates face crop view.
class FaceCropper: UIView {
    
    // - MARK: Properties
    
    /// Minimum value for the shorter side while resizing.
    private var size: CGFloat = 0
    /// Indicates default minimum size for resizing.
    private var defaultMinimumSize: CGFloat = 10
    public  var minimumSize: CGFloat {
        set {
            size = max(newValue, defaultMinimumSize)
        }
        get {
            return size
        }
    }
    
    /// Indicates the view initial bounds while resizing.
    private var initialBounds = CGRect.zero
    /// Indicates initial distance between CGPoint for rotation angle.
    private var initialDistance: CGFloat = 0
    /// Indicates delta angle.
    private var deltaAngle: CGFloat = 0
    /// Indicates crop angle after rotation.
    private var cropAngle: CGFloat = 0
    
    /// Returns a point in the FaceCropper superview to performe crop paste operations.
    var cropPasteCenter: CGPoint {
        return CGPoint(x: originTracker.x + ratioSpace, y: originTracker.y + ratioSpace)
    }
    
    /// Indicates currenct device state.
    private let device = UIDevice.current.userInterfaceIdiom
    /// Indicates ratio constraints constant spaces of shapeMask & cropMask views inside the FaceCropper view.
    private var ratioSpace: CGFloat {
        return device == .pad ? 30 : 20
    }
    
    /// Returns the FaceCropper default frame  size, according to the device state.
    private var defaultSize: CGSize {
        return device == .pad ? CGSize(width: 200, height: 200) : CGSize(width: 150, height: 150)
    }
    
    /// Indicates a center point of the FaceCropper in it's superview.
    var originTracker: CGPoint = .zero
    /// Indicates the FaceCropper superview.
    weak var superView: UIImageView?
    /// Indicates image crop mask.
    var cropMask = UIImageView()
    /// Indicates the cropMask name.
    var cropMaskName = ""
    /// Indicates shapeMask
    var shapeMask = UIImageView()
    /// indicates the shapeMask name.
    var shapeMaskName = ""
    /// Indicates the crop state.
    var cropped = false
    /// Indicates rotation state.
    private var rotated = false
    
    /// Crop rotator instance.
    let cropRotator = CropRotator()
    /// Indicates cropMover.
    let cropMover = CropMover()
    /// Indicates top cropper resizer.
    let topIndicator = CropIndicator()
    /// Indicates bottom cropper resizer.
    let bottomIndicator = CropIndicator()
    /// Indicates right cropper resizer.
    let rightIndicator = CropIndicator()
    /// Indicates left cropper resizer.
    let leftIndicator = CropIndicator()
    
    // - MARK: Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setProperties()
        setTools()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setProperties()
    }
    
    // - MARK: Actions
    
    /// Performes crop operation.
    func crop() {
        guard let superView = self.superView else { return }
        hideTools()
        cropMask.isHidden = false
        cropMask.frame.origin = cropPasteCenter
        cropMask.transform = CGAffineTransform.identity.rotated(by: cropAngle)
        
        superView.mask = cropMask
        shapeMask.image = superView.imageFromMask()
        shapeMask.transform = CGAffineTransform.identity.rotated(by: -cropAngle)
        superView.mask = nil
        cropped = true
        showTools()
    }
    
    /// Performes paste operation of cropped image, hidding unneeded components, leaving modified image.
    func paste() {
        cropMask.isHidden = true
        cropMover.isHidden = true
        cropRotator.isHidden = true
        topIndicator.isHidden = true
        bottomIndicator.isHidden = true
        rightIndicator.isHidden = true
        leftIndicator.isHidden = true
    }
    
    /// Sets the FaceCropper at the center of it's superview.
    /// - Parameters:
    ///    - superView: - Indicates the FaceCropper superview.
    ///    - mask: - String representation of cropping mask image from assets catalog.
    func set(in superView: UIImageView, with mask: String) {
        setCurrentMask(mask)
        let x = superView.frame.width / 2 - (defaultSize.width / 2)
        let y = superView.frame.height / 2 - (defaultSize.height / 2)
        frame = CGRect(x: x , y: y , width: defaultSize.width, height: defaultSize.height)
        superView.insertSubview(self, at: superView.subviews.endIndex)
        originTracker = frame.origin
        self.superView = superView
    }
    
    // - MARK: Private Actions
    
    /// Hides the FaceCropper tools.
    func hideTools() {
        shapeMask.isHidden = true
        cropMover.isHidden = true
        cropRotator.isHidden = true
        hideIndicators()
    }
    
    /// Shows the FaceCropper tools.
    func showTools() {
        shapeMask.isHidden = false
        cropMover.isHidden = false
        cropRotator.isHidden = false
        showIndicators()
    }
    /// Hides the view indicators.
    private func hideIndicators() {
        topIndicator.isHidden = true
        bottomIndicator.isHidden = true
        rightIndicator.isHidden = true
        leftIndicator.isHidden = true
    }
    /// Shows the view indicators.
    private func showIndicators() {
        topIndicator.isHidden = false
        bottomIndicator.isHidden = false
        rightIndicator.isHidden = false
        leftIndicator.isHidden = false
    }
    
    /// Sets the FaceCropper properties.
    private func setProperties() {
        backgroundColor = .clear
        isUserInteractionEnabled = true
        contentMode = .scaleAspectFit
        isOpaque = true
        clipsToBounds = true
        translatesAutoresizingMaskIntoConstraints = true
        cropMask.backgroundColor = .clear
        cropMask.isOpaque = true
        cropMask.isHidden = true
        cropMask.isUserInteractionEnabled = true
        cropMask.translatesAutoresizingMaskIntoConstraints = false
        cropMask.contentMode = .scaleAspectFit
        shapeMask.backgroundColor = .clear
        shapeMask.isUserInteractionEnabled = true
        shapeMask.isOpaque = true
        shapeMask.contentMode = .scaleAspectFit
        shapeMask.translatesAutoresizingMaskIntoConstraints = false
    }
    
    /// Sets constraints.
    /// - Parameters:
    ///    - subview: - Indicates the FaceCropper subview.
    private func setConstraints(_ subview: UIView) {
        let leading = subview.leadingAnchor.constraint(equalTo: leadingAnchor, constant: ratioSpace)
        let trailing = subview.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -ratioSpace)
        let top = subview.topAnchor.constraint(equalTo: topAnchor, constant: ratioSpace)
        let bottom = subview.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -ratioSpace)
        NSLayoutConstraint.activate([leading, trailing, top, bottom])
    }
    
    /// Sets current selected mask.
    /// - Parameters:
    ///    - mask: - Indicates current mask image name from assets catalog.
    private func setCurrentMask(_ face: String) {
        cropMaskName = "\(face)+mask"
        shapeMaskName = "\(face)+shape"
        shapeMask.image = UIImage(named: shapeMaskName)
        cropMask.image = UIImage(named: cropMaskName)
    }
    
    /// Sets the FaceCropper tools.
    private func setTools() {
        topIndicator.add(to: shapeMask, pinTo: .top, with: ratioSpace)
        bottomIndicator.add(to: shapeMask, pinTo: .bottom, with: ratioSpace)
        rightIndicator.add(to: shapeMask, pinTo: .right, with: ratioSpace)
        leftIndicator.add(to: shapeMask, pinTo: .left, with: ratioSpace)
        hideIndicators()
        setMasks()
        cropMover.add(to: self)
        cropRotator.add(to: self, with: ratioSpace)
        addGestures()
    }
    
    /// Adds the FaceCropper masks.
    private func setMasks() {
        addSubview(cropMask)
        addSubview(shapeMask)
        setConstraints(cropMask)
        setConstraints(shapeMask)
    }
    
    func addGestures() {
        let rotateGesture = UIPanGestureRecognizer(target: self, action: #selector(rotateGesture(_:)))
        cropRotator.addGestureRecognizer(rotateGesture)
        let dragGesture = UIPanGestureRecognizer(target: self, action: #selector(dragGesture(_:)))
        cropMover.addGestureRecognizer(dragGesture)
    }
    
    /// Rotate gesture recognizer target.
    @objc func rotateGesture(_ gesture: UIPanGestureRecognizer) {
        
        let location = gesture.location(in: superview)
        let center = self.center
        let angle = atan2(location.y - center.y, location.x - center.x)
        let angleDiff = deltaAngle - angle
        
        switch gesture.state {
        
        case .began:
            showIndicators()
            deltaAngle = atan2(location.y - center.y, location.x - center.x) - getAngle(transform: transform)
            initialBounds = bounds
            initialDistance = getDistance(from: center, to: location)
            
        case .changed:
            transform = CGAffineTransform(rotationAngle: -angleDiff)
            
            var scale = getDistance(from: center, to: location) / initialDistance
            let minimumScale = CGFloat(minimumSize) / min(initialBounds.size.width, initialBounds.size.height)
            scale = max(scale, minimumScale)
            let scaledBounds = rectScale(rect: initialBounds, wScale: scale, hScale: scale)
            bounds = scaledBounds
            setNeedsDisplay()
            cropAngle = -angleDiff
            
        case .ended:
            hideIndicators()
            rotated = true
            originTracker = originalFrame.origin
        default:
            break
        }
    }
    
    /// Drag gesture recognizer target.
    @objc func dragGesture(_ gesture: UIPanGestureRecognizer) {
        let translation = gesture.translation(in: superview)
        switch gesture.state {
        case .began:
            showIndicators()
            originTracker = center
        case .changed:
            center = CGPoint(x: translation.x + originTracker.x, y: translation.y + originTracker.y)
        case .ended:
            hideIndicators()
            if rotated {
                originTracker = originalFrame.origin
            }
            else { originTracker = CGPoint(x: frame.origin.x, y: frame.origin.y) }
        default:
            break
        }
    }
    
    
    /// gets rect scale.
    func rectScale(rect: CGRect, wScale: CGFloat, hScale: CGFloat) -> CGRect {
        return CGRect(x: rect.origin.x, y: rect.origin.y, width: rect.size.width * wScale, height: rect.size.height * hScale)
    }
    /// return cg float angle of transformation
    func getAngle(transform: CGAffineTransform) -> CGFloat {
        return atan2(transform.b, transform.a)
    }
    /// Gets distance from CGPoint to CGPoint.
    func getDistance(from point1: CGPoint, to point2:CGPoint) -> CGFloat {
        let fx = point2.x - point1.x
        let fy = point2.y - point1.y
        return sqrt(fx * fx + fy * fy)
    }
    
    
}

extension FaceCropper: UIGestureRecognizerDelegate {
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return  true
    }
}


