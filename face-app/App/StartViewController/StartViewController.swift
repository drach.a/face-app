//
//  StartViewController.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 30.06.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.
//

import UIKit

/// Indicates starting ViewController.
class StartViewController: UIViewController {
    
    // - MARK: Properties
    
    @IBOutlet weak var selectedImage: UIImageView!
    @IBOutlet weak var startNavBar: StartNavigationBar!
    @IBOutlet weak var startTabBar: StartTabBar!
    
    /// Indicates edit photo model.
    var photoModel = EditingPhotoModel()
    /// Indicates momento operator.
    let momentoOperator = MomentoOperator()
    /// Pickes images from library.
    var imagePicker = UIImagePickerController()
    
    // - MARK: Controlller LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNotificationObservers()
        selectedImage.contentMode = .scaleAspectFit
        imagePicker.delegate = self
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        startNavBar.setShadows()
        startTabBar.setShadows()
    }
    
    
    // - MARK: Private Actions
    
    /// Sets the NotificationCenter observers to listen action events.
    private func setNotificationObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(selectPhoto), name: Notification.Name("selectPhoto"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showShapes), name: Notification.Name("showShapes"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(clear), name: Notification.Name("clear"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(edit), name: Notification.Name("edit"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(swap), name: Notification.Name("swap"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(cropFace), name: Notification.Name("crop"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(pasteFace), name: Notification.Name("paste"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(save), name: Notification.Name("save"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(redo), name: Notification.Name("redo"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(undo), name: Notification.Name("undo"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(mirrorImage), name: Notification.Name("mirror"), object: nil)
    }
    
    // - MARK: The Notification Center Targets
    
    @objc private func mirrorImage() {
        photoModel.mirrorFace()
    }
    
    @objc private func redo() {
        guard !momentoOperator.isEmpty else { return }
        for face in selectedImage.subviews {
            face.removeFromSuperview()
        }
        momentoOperator.write(to: &photoModel, with: .redo)
        updateData()
    }
    
    @objc private func undo() {
        guard !momentoOperator.isEmpty else { return }
        for face in selectedImage.subviews {
            face.removeFromSuperview()
        }
        momentoOperator.write(to: &photoModel, with: .undo)
        updateData()
    }
    
   private func updateData() {
        for face in photoModel.croppedFaces {
            selectedImage.addSubview(face)
             face.showTools()
        }
    }
    /// Saves image to library.
    @objc private func save() {
        pasteFace()
        guard let image = selectedImage.takeoutImage() else { return }
        UIImageWriteToSavedPhotosAlbum(image, self, #selector(saveError), nil)
        showAlert()
    }
    
    /// Shows alert.
    private func showAlert() {
        let alert = UIAlertController(title: "Photo saved!", message: "", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            alert.dismiss(animated: true)
        }))
        present(alert, animated: true)
    }
    
    @objc private func saveError(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        return
    }
    
    /// Changes image between edited and original.
    @objc private func swap() {
        if selectedImage.image != nil {
        switch photoModel.state {
        case .edited:
            hideSubviewIfNeeded()
             selectedImage.image = photoModel.originalImage
            photoModel.changeState()
        case .original:
            hideSubviewIfNeeded()
             selectedImage.image = photoModel.editedImage
            photoModel.changeState()
        }
        }
    }
    
    
    @objc private func cropFace() {
        guard let cropper = photoModel.faceCropper,
              !cropper.cropped
        else { return }
        cropper.crop()
        photoModel.croppedFaces.append(cropper)
        momentoOperator.read(from: photoModel)
    }
    
    /// Paste operation.
    @objc private func pasteFace() {
        for face in photoModel.croppedFaces {
            if face.cropped {
                face.paste()
            }
        }
    }
    
    @objc private func selectPhoto() {
        imagePicker.sourceType = .photoLibrary
        imagePicker.modalPresentationStyle = .overFullScreen
        present(imagePicker, animated: true)
    }
    
    /// Deletes photo from screen.
    @objc private func clear() {
        if selectedImage.image != nil {
        selectedImage.image = photoModel.originalImage
        photoModel.editedImage = photoModel.originalImage
        }
        photoModel.croppedFaces = []
        photoModel.state = .edited
        momentoOperator.clean()
        for face in selectedImage.subviews {
            face.removeFromSuperview()
        }
    }
    
    /// Opens the PhotoEditorViewController, passing chosen image to edit.
    @objc private func edit() {
        if selectedImage.image != nil {
        pasteFace()
            if let cropp = photoModel.faceCropper,
               !cropp.cropped {
                photoModel.faceCropper?.removeFromSuperview()
            }
        photoModel.editedImage = selectedImage.takeoutImage()!
            photoModel.subOriginalImage = photoModel.editedImage
        let editor = PhotoEditorViewController(nibName: "PhotoEditorViewController", bundle: nil)
        editor.delegate = self
        editor.modalPresentationStyle = .fullScreen
        present(editor, animated: true)
    }
    }
    
    // Opens the PhotoEditorViewController, passing chosen image to edit.
    @objc private func showShapes() {
        if selectedImage.image != nil {
        let shapesController = ShapesViewController(nibName: "ShapesViewController", bundle: nil)
        present(shapesController, animated: true) { [self] in
            shapesController.shapesCollection.delegate = self
        }
        }
    }
    
    // - MARK: Actions
    
    /// Adds a new FaceCropper to the view.
    /// - Parameters:
    ///   - mask: - Name of cropping mask.
    func add(with mask: String) {
        if photoModel.faceCropper == nil || photoModel.faceCropper!.cropped {
            photoModel.faceCropper = FaceCropper()
            photoModel.faceCropper?.set(in: selectedImage, with: mask)
        }
        else {
            photoModel.faceCropper?.removeFromSuperview()
            photoModel.faceCropper = FaceCropper()
            photoModel.faceCropper?.set(in: selectedImage, with: mask)
        }
    }
    
    /// Does cleaning stuff .
    func cleanup() {
        for face in photoModel.croppedFaces {
            face.removeFromSuperview()
        }
        photoModel.faceCropper?.removeFromSuperview()
    }
    
    
    /// Hides subviews.
    func hideSubviewIfNeeded() {
        for subView in selectedImage.subviews {
            if subView.isHidden {
                subView.isHidden = false
            }
            else { subView.isHidden = true }
        }
    }
    
    
}
