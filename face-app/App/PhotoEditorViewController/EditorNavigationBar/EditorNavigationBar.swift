//
//  EditorNavigationBar.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 02.07.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import UIKit

/// Indicates the PhotoEditorViewController navigationBar.
class EditorNavigationBar: UIView {
    
    // - MARK: Structs
    
    /// Indicates the NavBar state.
    enum State: String, Equatable, CaseIterable {
        case standart = "Photo Editor"
        case text = "Add Text"
        case draw = "Draw"
        case effects = "Effects"
        case orientation = "Orientation"
        case crop = "Crop"
        case lighting = "Lighting"
        case color = "Color"
        case vignette = "Vignette"
        case blur = "Blur"
    }
    
    // - MARK: Properties
    
    /// Indicates current the NavBar state.
    var state: State = .standart
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var doneButtonOutlet: UIButton!
    @IBOutlet weak var cancelButtonOutlet: UIButton!
    
    // - MARK: Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        loadFromNib(name: "EditorNavigationBar")
    }
    
    // - MARK: Actions
    
    /// Sets the bar title.
    func setTitle(_ title: State) {
        state = title
        let doneTitle = title == .standart ? "Done" : "Apply"
        doneButtonOutlet.setTitle(doneTitle, for: .normal)
        titleLabel.text = title.rawValue
    }
    
    // - MARK: Private Actions
    
    @IBAction private func doneAction(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name("editorDone"), object: nil)
    }
    
    @IBAction private func cancelAction(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name("editorCancel"), object: nil)
    }
    
}
