//
//  FontCell.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 02.09.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import UIKit

/// Indicates font cell.
class FontCell: UITableViewCell {
    
    // - MARK: Properties
    
    private let device = UIDevice.current.userInterfaceIdiom
    
    @IBOutlet private weak var label: UILabel!
    @IBOutlet private weak var view: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: true)
        view.isHidden = selected ? false : true
    }
    
    /// Sets the cell font components.
    func setComponents(from font: UIFont) {
        let fontSize: CGFloat = device == .phone ? 24 : 30
        label.font = font.withSize(fontSize)
        label.text = font.fontName
    }
    
   
    
   
    
}
