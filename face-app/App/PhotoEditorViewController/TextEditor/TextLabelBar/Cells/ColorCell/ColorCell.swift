//
//  ColorCell.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 02.09.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import UIKit

/// Indicates color cell.
class ColorCell: UICollectionViewCell {
    
    // - MARK: Properties
    @IBOutlet weak var view: UIImageView!
    
    private var color: UIColor = .clear
    
    // - MARK: Initialization
    override func awakeFromNib() {
        super.awakeFromNib()
        view.backgroundColor = .clear
        backgroundColor = .clear
    }
    
    // - MARK: Actions
    
    /// Sets the cell content.
    func setContent(from color: UIColor) {
        view.image = UIImage(named: "UnselectedColorCell")?.withTintColor(color)
        self.color = color
    }
    
    func setSelected() {
        view.image = UIImage(named: "SelectedColorCell")?.withTintColor(color)
    }
    
    func setUnselected() {
        view.image = UIImage(named: "UnselectedColorCell")?.withTintColor(color)
    }

}
