//
//  TexLabelBar+Extensions.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 02.09.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import UIKit

// - MARK: UITableViewDelegate & DataSource
extension TextLabelBar: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fonts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "FontCell", for: indexPath) as? FontCell,
              let font = fonts[indexPath.row]
        else { fatalError("Cannot load FontCell with font: \(fonts[indexPath.row]!.fontName)") }
        cell.setComponents(from: font)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let font = fonts[indexPath.row] else { return }
        delegate?.textBar(self, didSelectFont: font.withSize(20))
    }
}

// - MARK: UICollectionViewDelegate & DataSource
extension TextLabelBar: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return colors.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ColorCell", for: indexPath) as? ColorCell
        else { fatalError("Cannot dequeue cell for indexPath: \(indexPath)!") }
        if colors[indexPath.row] != colors.last {
            cell.setContent(from: colors[indexPath.row])
        }
        else {
            cell.view.image = UIImage(named: "addColorIcon")
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? ColorCell else { return }
        if colors[indexPath.row] != colors.last {
        cell.setSelected()
        delegate?.textBar(self, didSelectColor: colors[indexPath.row])
        }
        else {
            NotificationCenter.default.post(name: Notification.Name("presentColorPicker"), object: nil)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? ColorCell else { return }
        if colors[indexPath.row] != colors.last {
        cell.setUnselected()
        }
    }
    
}

// - MARK: UICollectionViewDelegateFlowLayout
extension TextLabelBar: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        /// Indicates each item size to return.
        var size: CGSize = CGSize()
        
        // Calculating width:
        let width = frame.width - (collectionView.contentInset.left + collectionView.contentInset.right)
        size.width = width / 7
        size.height = size.width
        
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}

// - MARK: UIColorPickerViewControllerDelegate
extension TextLabelBar: UIColorPickerViewControllerDelegate {
    
    func colorPickerViewControllerDidFinish(_ viewController: UIColorPickerViewController) {
        colors.insert(viewController.selectedColor, at: colors.count - 1)
        colorCollection?.reloadData()
    }
    
}
