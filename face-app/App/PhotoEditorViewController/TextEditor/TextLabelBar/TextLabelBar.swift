//
//  TextLabelBar.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 01.09.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import UIKit

/// Indicates the TextLabelBar.
class TextLabelBar: UIView {
    
    /// The bar state.
    enum State: String, Equatable, CaseIterable {
        case color = "selectedColor"
        case font = "selectedFont"
        case keyboard = "selectedKeyboard"
    }
    
    // - MARK: Properties
    @IBOutlet weak var fontButton: UIButton!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var colorButton: UIButton!
    @IBOutlet weak var keyboardButton: UIButton!
    
    weak var delegate: TextBarDelegate?
    /// Picker color.
    let colorPicker = UIColorPickerViewController()
    /// Indicates font  tableView.
    let fontTable = UITableView()
    var fonts = [UIFont(name: "American Typewriter", size: 16), UIFont(name: "Futura Medium", size: 16), UIFont(name: "Georgia", size: 16), UIFont(name: "Gill Sans", size: 16), UIFont(name: "Gill Sans Italic", size: 16), UIFont(name: "Helvetica", size: 16), UIFont(name: "Helvetica Neue", size: 16), UIFont(name: "Marcellus", size: 16), UIFont(name: "Marker Felt Wide", size: 16), UIFont(name: "Optima Bold", size: 16)]
    
    /// Indicates colorCollectionView
    var colorCollection: UICollectionView?
    /// Colors for the collection.
    var colors: [UIColor] = [#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), #colorLiteral(red: 0.007763293106, green: 0.3268813491, blue: 0.1449773908, alpha: 1), #colorLiteral(red: 0, green: 0.4144335985, blue: 0.1767787635, alpha: 1), #colorLiteral(red: 0.1280970275, green: 0.5889706612, blue: 0.3276189268, alpha: 1), #colorLiteral(red: 0.1499820054, green: 0.6804082394, blue: 0.3783406019, alpha: 1), #colorLiteral(red: 0.4357878566, green: 0.812977612, blue: 0.5901198387, alpha: 1), #colorLiteral(red: 0.1459098756, green: 0.8157631755, blue: 0.7364478111, alpha: 1), #colorLiteral(red: 0.1180937961, green: 0.788026154, blue: 0.8793967366, alpha: 1), #colorLiteral(red: 0.3397654295, green: 0.7983145714, blue: 0.9496322274, alpha: 1), #colorLiteral(red: 0.10066659, green: 0.6687210798, blue: 0.857529223, alpha: 1), #colorLiteral(red: 0.1830454767, green: 0.5009093881, blue: 0.9273914695, alpha: 1), #colorLiteral(red: 0, green: 0.3707507849, blue: 0.8658934832, alpha: 1), #colorLiteral(red: 0, green: 0.1321426928, blue: 0.825097084, alpha: 1), #colorLiteral(red: 0.1735287309, green: 0.02130087838, blue: 0.6051145196, alpha: 1), #colorLiteral(red: 0.6066625118, green: 0.3186083436, blue: 0.8762715459, alpha: 1), #colorLiteral(red: 0.7327192426, green: 0.4201564193, blue: 0.8511666656, alpha: 1), #colorLiteral(red: 0.8778762221, green: 0.5832000971, blue: 0.9927117229, alpha: 1), #colorLiteral(red: 0.9428401589, green: 0.7863597274, blue: 0.998513639, alpha: 1), #colorLiteral(red: 1, green: 0.7877735496, blue: 0.9422179461, alpha: 1), #colorLiteral(red: 0.9543091655, green: 0.4889386296, blue: 0.8236032724, alpha: 1), #colorLiteral(red: 0.9272651672, green: 0.06432252377, blue: 0.6878793836, alpha: 1), #colorLiteral(red: 0.9220944047, green: 0.3408394158, blue: 0.3441366553, alpha: 1), #colorLiteral(red: 0.95156461, green: 0.599634707, blue: 0.2884348631, alpha: 1), #colorLiteral(red: 0.9474667907, green: 0.7861401439, blue: 0.2961711287, alpha: 1), #colorLiteral(red: 0.9996564984, green: 0.8575732112, blue: 0.3542559147, alpha: 1), #colorLiteral(red: 1, green: 0.918677032, blue: 0.182197392, alpha: 1), #colorLiteral(red: 0.9999018312, green: 1, blue: 0.9998798966, alpha: 1), #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)]
    
    // - MARK: Initialization
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setProperties()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setProperties()
    }
    
    
    // - MARK: Actions
    
    func addTo(view: UIView, with size: CGSize) {
        let width: CGFloat = size.width
        let height: CGFloat = size.height + 50
        let x = view.frame.width / 2 - (width / 2)
        let y = view.frame.maxY - height
        frame = CGRect(x: x, y: y, width: width, height: height)
        view.addSubview(self)
    }
    
    // - MARK: Private Actions
    
    private func setProperties() {
        isHidden = true
        colorPicker.delegate = self
        colorPicker.isModalInPresentation = true
        colorPicker.modalPresentationStyle = .fullScreen
        loadFromNib(name: "TextLabelBar")
        fontTable.register(UINib(nibName:"FontCell", bundle: nil), forCellReuseIdentifier: "FontCell")
    }
    
    private func setColorCollection() {
        
        let layout = UICollectionViewFlowLayout()
        //  layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        // layout.itemSize = CGSize(width: 50, height: 50)
        layout.scrollDirection = .vertical
        colorCollection = UICollectionView(frame: .zero, collectionViewLayout: layout)
        colorCollection?.translatesAutoresizingMaskIntoConstraints = false
        colorCollection?.backgroundColor = .clear
        colorCollection?.alwaysBounceVertical = true
        colorCollection?.register(UINib(nibName:"ColorCell", bundle: nil), forCellWithReuseIdentifier: "ColorCell")
        addSubview(colorCollection!)
        colorCollection?.topAnchor.constraint(equalTo: topAnchor, constant: 50).isActive = true
        colorCollection?.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        colorCollection?.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        colorCollection?.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        colorCollection?.dataSource = self
        colorCollection?.delegate = self
        
    }
    
    
    private func setFontTable() {
        fontTable.translatesAutoresizingMaskIntoConstraints = false
        fontTable.separatorColor = .clear
        fontTable.backgroundColor = .clear
        addSubview(fontTable)
        fontTable.topAnchor.constraint(equalTo: topAnchor, constant: 50).isActive = true
        fontTable.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        fontTable.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        fontTable.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        fontTable.dataSource = self
        fontTable.delegate = self
    }
    
    // Sets image for a button.
    private func setImage(for button: State) {
        switch button {
        case .color:
            colorButton.setImage(UIImage(named: button.rawValue), for: .normal)
            fontButton.setImage(UIImage(named: "selectFont"), for: .normal)
            keyboardButton.setImage(UIImage(named: "keyboard"), for: .normal)
        case .font:
            fontButton.setImage(UIImage(named: button.rawValue), for: .normal)
            colorButton.setImage(UIImage(named: "selectColor"), for: .normal)
            keyboardButton.setImage(UIImage(named: "keyboard"), for: .normal)
        case .keyboard:
            keyboardButton.setImage(UIImage(named: button.rawValue), for: .normal)
            fontButton.setImage(UIImage(named: "selectFont"), for: .normal)
            colorButton.setImage(UIImage(named: "selectColor"), for: .normal)
        }
    }
    
    
    // - MARK: Private IBActions
    
    @IBAction private func fontButtonAction(_ sender: Any) {
        delegate?.textBar(self, shouldHideKeyboard: true)
        setImage(for: .font)
        colorCollection?.removeFromSuperview()
        setFontTable()
    }
    
    @IBAction private func doneButtonAction(_ sender: Any) {
        delegate?.textBar(self, doneEditing: true)
    }
    
    @IBAction private func colorButtonAction(_ sender: Any) {
        delegate?.textBar(self, shouldHideKeyboard: true)
        setImage(for: .color)
        fontTable.removeFromSuperview()
        setColorCollection()
    }
    
    @IBAction private func keyboardButtonAction(_ sender: Any) {
        delegate?.textBar(self, shouldHideKeyboard: false)
        setImage(for: .keyboard)
        colorCollection?.removeFromSuperview()
        fontTable.removeFromSuperview()
    }
    
}
