//
//  TextBarDelegate.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 01.09.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import UIKit

/// Delegates the TextLabelBar actions.
protocol TextBarDelegate: AnyObject {
    
    /// Tells about: the TextLabelBar pressed done button..
    /// - Parameters:
    ///     - textBar: Indicates  the TextLabel toolbar object.
    ///     - finished: Indicates bool value of finishing photo editing.
    func textBar(_ textBar: TextLabelBar, doneEditing isDone: Bool)
    
    /// Tells about: the TextLabelBar pressed keyboard button.
    /// - Parameters:
    ///     - textBar: Indicates  the TextLabel toolbar object.
    ///     - finished: Indicates bool value of finishing photo editing.
    func textBar(_ textBar: TextLabelBar, keyboardPressed: Bool)
    
    /// Tells about: the TextLabelBar selected textColor.
    /// - Parameters:
    ///     - textBar: Indicates  the TextLabel toolbar object.
    ///     - color: Indicates textColor
    func textBar(_ textBar: TextLabelBar, didSelectColor textColor: UIColor)
    
    /// Tells about: the TextLabelBar selected textFont.
    /// - Parameters:
    ///     - textBar: Indicates  the TextLabel toolbar object.
    ///     - textFont: Indicates textFont.
    func textBar(_ textBar: TextLabelBar, didSelectFont textFont: UIFont)
    
    /// Tells about: the TextLabelBar selected textFont.
    /// - Parameters:
    ///     - textBar: Indicates  the TextLabel toolbar object.
    ///     - textFont: Indicates textFont.
    func textBar(_ textBar: TextLabelBar, shouldHideKeyboard should: Bool)

}
