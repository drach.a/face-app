//
//  AddText.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 31.08.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import UIKit

/// Indicates add text label button.
class AddText: UIView {
    
    // - MARK: Properties
    
    @IBOutlet weak var addText: UIButton!
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setProperties()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setProperties()
    }
    
    // - MARK: Actions
    func addTo(view: UIView) {
        let width: CGFloat = 300
        let height: CGFloat = 50
        let x = view.frame.width / 2 - (width / 2)
        let y = view.frame.height / 2 - (height / 2)
        frame = CGRect(x: x, y: y, width: width, height: height)
        view.addSubview(self)
    }
    
    // - MARK: Private Actions
    
   private func setProperties() {
        loadFromNib(name: "AddText")
        backgroundColor = .clear
        addText.layer.masksToBounds = true
        addText.layer.cornerRadius = 10
    }
    
    @IBAction func addTextAction(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name("addText"), object: nil)
    }
    
}
