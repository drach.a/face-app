//
//  TextLabel+Extensions.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 01.09.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import UIKit

extension TextLabel: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            labelDone()
            return false
        }
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
    }
    
    
    func textViewDidChange(_ textView: UITextView) {
        if textView.isTruncated {
            frame = CGRect(origin: frame.origin, size: CGSize(width: frame.width, height: frame.height + 40))
        }
    }
    
}

// - MARK: UIGestureRecognizerDelegate
extension TextLabel: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return  true
    }
}

// - MARK: TextBarDelegate
extension TextLabel: TextBarDelegate {
    
    func textBar(_ textBar: TextLabelBar, doneEditing isDone: Bool) {
        labelDone()
    }
    
    func textBar(_ textBar: TextLabelBar, keyboardPressed: Bool) {
        label.becomeFirstResponder()
    }
    
    func textBar(_ textBar: TextLabelBar, didSelectColor textColor: UIColor) {
        label.textColor = textColor
    }
    
    func textBar(_ textBar: TextLabelBar, didSelectFont textFont: UIFont) {
        
        label.font = textFont
    }
    
    func textBar(_ textBar: TextLabelBar, shouldHideKeyboard should: Bool) {
        if should {
            label.resignFirstResponder()
        }
        else { label.becomeFirstResponder() }
    }
    
}

// - MARK: UItextView
extension UITextView {
    /// Indicates text trunctation.
    var isTruncated: Bool {
        var isTruncating = false
        layoutManager.enumerateLineFragments(forGlyphRange: NSRange(location: 0, length: Int.max)) { _, _, _, glyphRange, stop in
            let truncatedRange = self.layoutManager.truncatedGlyphRange(inLineFragmentForGlyphAt: glyphRange.lowerBound)
            if truncatedRange.location != NSNotFound {
                isTruncating = true
                stop.pointee = true
            }
        }
        
        if isTruncating == false {
            let glyphRange = layoutManager.glyphRange(for: textContainer)
            let characterRange = layoutManager.characterRange(forGlyphRange: glyphRange, actualGlyphRange: nil)
            
            isTruncating = characterRange.upperBound < text.utf16.count
        }
        return isTruncating
    }
}

