//
//  TextLabel.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 31.08.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import UIKit

/// Indicates text label.
class TextLabel: UIView {
    
    // - MARK: Properties
    
    @IBOutlet weak var label: UITextView!
    @IBOutlet weak var remover: UIButton!
    @IBOutlet weak var rotator: UIImageView!
    @IBOutlet weak var mover: UIImageView!
    
    var firstTransform = CGFloat()
    
    /// Indicates current device.
    private let device = UIDevice.current.userInterfaceIdiom
    
    /// Minimum value for the shorter side while resizing.
    private var size: CGFloat = 0
    /// Indicates default minimum size for resizing.
    private var defaultMinimumSize: CGFloat = 10
    public  var minimumSize: CGFloat {
        set {
            size = max(newValue, defaultMinimumSize)
        }
        get {
            return size
        }
    }
    
    /// Indicates the view initial bounds while resizing.
    private var initialBounds = CGRect.zero
    /// Indicates initial distance between CGPoint for rotation angle.
    private var initialDistance: CGFloat = 0
    /// Indicates delta angle.
    private var deltaAngle: CGFloat = 0
    /// Indicates a center point of the FaceCropper in it's superview.
    var originTracker: CGPoint = .zero
    /// Indicates the view ToolBar availability.
    var toolBarIsSet = false
    /// Indicates textLabelBar
    var textBar: TextLabelBar?
    
    // - MARK: Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setProperties()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setProperties()
    }
    
    // - MARK: Actions
    
    /// Sets the TextLabel at the center of it's superview.
    /// - Parameters:
    ///    - superView: - Indicates the view superview.
    func set(in superView: UIImageView) {
        let width: CGFloat = device == .phone ? 200 : 230
        let height: CGFloat = 70
        let x = superView.frame.width / 2 - (width / 2)
        let y = superView.frame.height / 4
        frame = CGRect(x: x , y: y , width: width, height: height)
        superView.addSubview(self)
    }
    
    func finishEditing() {
        textBar?.removeFromSuperview()
        textBar = nil
        label.resignFirstResponder()
        label.backgroundColor = .clear
        label.isEditable = false
        label.isSelectable = false
        rotator.isHidden = true
        remover.isHidden = true
        mover.isHidden = true
    }
    
    func labelDone() {
        textBar?.removeFromSuperview()
        textBar = nil
        label.backgroundColor = .clear
        label.resignFirstResponder()
        label.isEditable = false
        label.isSelectable = false
        remover.isHidden = true
        rotator.isHidden = true
        mover.isHidden = true
    }
    
    
    // - MARK: Private Actions
    
    private func setProperties() {
        loadFromNib(name: "TextLabel")
        translatesAutoresizingMaskIntoConstraints = true
        label.delegate = self
        label.translatesAutoresizingMaskIntoConstraints = false
        label.sizeToFit()
        label.layer.masksToBounds = true
        label.layer.cornerRadius = 8
        label.textContainer.lineBreakMode = .byTruncatingTail
        addGestures()
    }
    
    private func addGestures() {
        let dragGesture = UIPanGestureRecognizer(target: self, action: #selector(dragGesture(_:)))
        mover.addGestureRecognizer(dragGesture)
        
        let pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(pinchGesture(_:)))
        addGestureRecognizer(pinchGesture)
        let rotateGesture = UIPanGestureRecognizer(target: self, action: #selector(rotateGesture(_:)))
        rotator.addGestureRecognizer(rotateGesture)
        addGestureRecognizer(dragGesture)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGesture(_:)))
        addGestureRecognizer(tapGesture)
    }
    
    @objc private func tapGesture(_ gesture: UITapGestureRecognizer) {
        if !label.isEditable && gesture.state == .ended {
            if label.backgroundColor == .clear {
                label.backgroundColor = #colorLiteral(red: 0.3529411765, green: 0.7843137255, blue: 0.9803921569, alpha: 0.5433826573)
                remover.isHidden = false
            }
            else { label.backgroundColor = .clear
                remover.isHidden = true
            }
        }
    }
    
    
    @objc private func pinchGesture(_ gesture: UIPinchGestureRecognizer) {
        if !label.isEditable {
            transform = CGAffineTransform(scaleX: gesture.scale, y: gesture.scale).rotated(by: firstTransform)
        }
    }
    
    /// Drag gesture recognizer target.
    @objc private func dragGesture(_ gesture: UIPanGestureRecognizer) {
        let translation = gesture.translation(in: superview)
        switch gesture.state {
        case .began:
            originTracker = center
        case .changed:
            center = CGPoint(x: translation.x + originTracker.x, y: translation.y + originTracker.y)
        case .ended:
            originTracker = CGPoint(x: frame.origin.x, y: frame.origin.y)
            
        default:
            break
        }
    }
    
    /// Rotate gesture recognizer target.
    @objc func rotateGesture(_ gesture: UIPanGestureRecognizer) {
        
        let location = gesture.location(in: superview)
        let center = self.center
        let angle = atan2(location.y - center.y, location.x - center.x)
        let angleDiff = deltaAngle - angle
        
        switch gesture.state {
        
        case .began:
            deltaAngle = atan2(location.y - center.y, location.x - center.x) - getAngle(transform: transform)
            initialBounds = bounds
            initialDistance = getDistance(from: center, to: location)
            
        case .changed:
            transform = CGAffineTransform(rotationAngle: -angleDiff)
            
            var scale = getDistance(from: center, to: location) / initialDistance
            let minimumScale = CGFloat(minimumSize) / min(initialBounds.size.width, initialBounds.size.height)
            scale = max(scale, minimumScale)
            let scaledBounds = rectScale(rect: initialBounds, wScale: scale, hScale: scale)
            bounds = scaledBounds
            setNeedsDisplay()
            
        case .ended:
            originTracker = originalFrame.origin
            firstTransform = -angleDiff
        default:
            break
        }
    }
    
    /// gets rect scale.
    func rectScale(rect: CGRect, wScale: CGFloat, hScale: CGFloat) -> CGRect {
        return CGRect(x: rect.origin.x, y: rect.origin.y, width: rect.size.width * wScale, height: rect.size.height * hScale)
    }
    /// return cg float angle of transformation
    func getAngle(transform: CGAffineTransform) -> CGFloat {
        return atan2(transform.b, transform.a)
    }
    /// Gets distance from CGPoint to CGPoint.
    func getDistance(from point1: CGPoint, to point2:CGPoint) -> CGFloat {
        let fx = point2.x - point1.x
        let fy = point2.y - point1.y
        return sqrt(fx * fx + fy * fy)
    }
    
    @IBAction private func removeAction(_ sender: Any) {
        textBar?.removeFromSuperview()
        textBar = nil
        removeFromSuperview()
    }
    
}
