//
//  OptionReader.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 04.08.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

/// Reads editing options.
struct OptionReader {
    
    
    // - MARK: Options
   
    /// Indicates filter effects options.
    enum Effects: String, Equatable, CaseIterable {
        case original = "Original"
        case sepia = "Sepia"
        case bloom = "Bloom"
        case chrome = "Chrome"
        case vibrance = "Vibrance"
        case hue = "Hue"
        case gamma = "Gamma"
        case exposure = "Exposure"
        case zoomBlur = "ZoomBlur"
        case scale = "Scale"
        case flight = "Flight"
    }
    
    /// Indicates  orientation options.
    enum Orientation: String, Equatable, CaseIterable {
        case left = "Left"
        case right = "Right"
        case vertical = "Vertical"
        case horizontal = "Horizontal"
    }
    
    /// Indicates crop options.
    enum Crop: String, Equatable, CaseIterable {
        case custom = "Custom"
        case original = "Original"
        case square = "Square"
        case four_three = "Four_three"
        case six_four = "Six_four"
        case seven_five = "Seven_five"
        case ten_eight = "Ten_eight"
        case sixteen_nine = "Sixteen_nine"
    }
    
    /// Indicates  lighting options.
    enum Lighting: String, Equatable, CaseIterable {
        case brightness = "Brightness"
        case contrast = "Contrast"
        case highlights = "Highlights"
        case shadows = "Shadows"
    }
    
    /// Indicates color options.
    enum Color: String, Equatable, CaseIterable {
        case saturation = "Saturation"
        case warmth = "Warmth"
        case fade = "Fade"
        case tint = "Tint"
    }
    
}
