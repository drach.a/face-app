//
//  EditorOptions.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 02.08.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import Foundation

/// Indicates the EditorCollectionTabBar options structure.
struct EditorOptions {
    
    // - MARK: Structs
    
    /// Indicates edit options..
    enum OptionState: String, Equatable, CaseIterable {
        case standart
        case text = "Text"
        case draw = "Draw"
        case effects = "Effects"
        case orientation = "Orientation"
        case crop = "Crop"
        case lighting = "Lighting"
        case color = "Color"
        case vignette = "Vignette"
        case blur = "Blur"
    }
    
    // - MARK: Properties
    
    /// Indicates option state, by default it's standard.
    var state: OptionState = .standart
   
    /// Returns array of options according to selected option..
    var options: [String] {
        switch state {
    
        case .standart:
            return EditorOptions.standartOptions
        case .text:
            return []
        case .draw:
            return []
        case .effects:
            return EditorOptions.effectsOptions
        case .orientation:
            return EditorOptions.orientationOptions
        case .crop:
            return EditorOptions.cropOptions
        case .lighting:
            return EditorOptions.lightingOptions
        case .color:
            return EditorOptions.colorOptions
        case .vignette:
            return []
        case .blur:
            return []
        }
    }
    
    /// Indicates the EditorCollectionTabBar first loaded options.
    private static let standartOptions: [String] = ["Text", "Draw", "Effects", "Orientation", "Crop", "Lighting", "Color", "Vignette", "Blur"]
    
    /// Indicates  the Crop option items.
    private static let cropOptions: [String] = ["Custom", "Original", "Square", "Four_three", "Six_four", "Seven_five", "Ten_eight", "Sixteen_nine"]
    
    /// Indicates the EditorCollection Color  Items.
    private static let colorOptions: [String] = ["Saturation", "Warmth", "Fade", "Tint"]
    
    /// Indicates the EditorCollection Effect filters.
    private static let effectsOptions: [String] = ["Original", "Sepia", "Bloom", "Chrome", "Vibrance", "Hue", "Gamma", "Exposure", "ZoomBlur", "Scale", "Flight"]
    
    /// Indicates the EditorCollection Lighting Items.
    private static let lightingOptions: [String] = ["Brightness", "Contrast", "Highlights", "Shadows"]
    
    /// Indicates the EditorCollection Orientation Items.
    private static let orientationOptions: [String] = ["Left", "Right", "Vertical", "Horizontal"]
    
   
    // - MARK: Actions
    
    /// Sets the optionState by it's rawValue.
    mutating func setState(by rawValue: String) {
        guard let state = OptionState(rawValue: rawValue) else { return }
        self.state = state
    }
    
}
