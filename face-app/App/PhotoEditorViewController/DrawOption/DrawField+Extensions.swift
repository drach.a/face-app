//
//  DrawField+Extensions.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 03.09.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import UIKit


// - MARK: DrawBarDelegate
extension DrawField: DrawBarDelegate {
    // When size selected.
    func drawBar(_ drawBar: DrawBar, didSelectSize drawSize: CGFloat) {
        self.drawSize = drawSize
    }
    // When color selected.
    func drawBar(_ drawBar: DrawBar, didPickColor drawColor: UIColor) {
        self.drawColor = drawColor
    }
    // When drawing state selected.
    func drawBar(_ drawBar: DrawBar, isDrawing: Bool) {
        if isDrawing {
            setDraw()
        }
        else {
            setErase()
        }
    }
}
