//
//  DrawField.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 03.09.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import UIKit

/// Indicates a view filed to draw in.
class DrawField: UIView {
    
    /// Indicates the view state.
    enum State {
        case draw
        case erase
    }
    
    // - MARK: Properties
    
    /// Indicates current view state.
    var state: State = .draw
    /// Indicates draw color.
    var drawColor: UIColor = .black
    /// Indicates draw width.
    var drawSize: CGFloat = 3
    /// Indicates the view draw bar.
    let drawBar = DrawBar()
    
    /// indicates UiImageView to render drawables into UiImage.
    let imageView = UIImageView()
    /// Indicates an array of drawing lines.
    private var lines: [Line] = []
    
    // - MARK: Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setProperties()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    // - MARK: Actions
    
    /// Adds the draw fiels toi t's superview.
    func addTo(view: UIView) {
        view.addSubview(self)
        topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        addSubview(imageView)
        imageView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        imageView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        imageView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        imageView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
    }
    
    /// Sets draw state
    func setDraw() {
        state = .draw
        imageView.isUserInteractionEnabled = false
    }
    /// Sets arase state.
    func setErase() {
        state = .erase
        imageView.image = uiImage()
        imageView.isUserInteractionEnabled = true
        clear()
    }
    
    // - MARK: Private Actions
    
    /// Clears drawing lines.
    private func clear() {
        lines.removeAll()
        setNeedsDisplay()
    }
    
    /// Sets properties of the view.
    private func setProperties() {
        translatesAutoresizingMaskIntoConstraints = false
        isUserInteractionEnabled = true
        contentMode = .scaleToFill
        backgroundColor = .clear
        isOpaque = true
        drawBar.delegate = self
        
        imageView.isOpaque = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.isUserInteractionEnabled = false
        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleToFill
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panGesture(_:)))
        imageView.addGestureRecognizer(panGesture)
    }
    
    @objc private func panGesture(_ sender: UIPanGestureRecognizer) {
        if state == .erase {
            
            let currentPoint = sender.location(in: imageView)
            UIGraphicsBeginImageContextWithOptions(imageView.frame.size, false, 0)
            let context = UIGraphicsGetCurrentContext()
            context?.setLineWidth(drawSize * 2)
            context?.setLineCap(.round)
            sender.view?.layer.render(in: context!)
            
            context?.setBlendMode(.clear)
            context?.move(to: currentPoint)
            context?.addLine(to: currentPoint)
            context?.strokePath()
            
            imageView.image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
        }
    }
    
    // - MARK: Touches
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        if state == .draw {
            guard let context = UIGraphicsGetCurrentContext() else { return }
            
            lines.forEach { (line) in
                context.setStrokeColor(line.drawColor.cgColor)
                context.setLineWidth(line.drawWidth)
                context.setLineCap(.round)
                for (i, p) in line.points.enumerated() {
                    if i == 0 {
                        context.move(to: p)
                    } else {
                        context.addLine(to: p)
                    }
                }
                context.strokePath()
            }
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if state == .draw {
            lines.append(Line(drawWidth: drawSize, drawColor: drawColor, points: []))
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if state == .draw {
            guard let point = touches.first?.location(in: self) else { return }
            guard var lastLine = lines.popLast() else { return }
            lastLine.points.append(point)
            lines.append(lastLine)
            setNeedsDisplay()
        }
    }
    
    
}
