//
//  Line.swift
//  face-app
//
//  Created by wouff on 08.09.2021.
//

import UIKit

/// indicates draw line.
struct Line {
    
    let drawWidth: CGFloat
    let drawColor: UIColor
    var points: [CGPoint]
}
