//
//  DrawColorCell.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 03.09.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import UIKit

/// Indicates a cell for color.
class DrawColorCell: UICollectionViewCell {
    
    // - MARK: Properties.
    @IBOutlet weak var view: UIImageView!
    
    private var color: UIColor = .clear
    
    // - MARK: Initialization
    
    override func awakeFromNib() {
        super.awakeFromNib()
        view.backgroundColor = .clear
        backgroundColor = .clear
    }
    
    // - MARK: Actions
    
    func setContent(from color: UIColor) {
        view.image = UIImage(named: "UnselectedColorCell")?.withTintColor(color)
        self.color = color
    }
    
    func setSelected() {
        view.image = UIImage(named: "SelectedColorCell")?.withTintColor(color)
    }
    
    func setUnselected() {
        view.image = UIImage(named: "UnselectedColorCell")?.withTintColor(color)
    }
    
}
