//
//  DrawBar+Extension.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 03.09.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import UIKit

// - MARK: UICollectionViewDelegate & DataSource
extension DrawBar: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return colors.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DrawColorCell", for: indexPath) as? DrawColorCell
        else { fatalError("Cannot dequeue cell for indexPath: \(indexPath)!") }
        if colors[indexPath.row] != colors.last {
            cell.setContent(from: colors[indexPath.row])
        }
        else {
            cell.view.image = UIImage(named: "addColorIcon")
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? DrawColorCell else { return }
        if colors[indexPath.row] != colors.last {
        cell.setSelected()
            delegate?.drawBar(self, didPickColor: colors[indexPath.row])
        }
        else {
            NotificationCenter.default.post(name: Notification.Name("presentColorPicker"), object: nil)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? DrawColorCell else { return }
        if colors[indexPath.row] != colors.last {
        cell.setUnselected()
        }
    }
    
}

// - MARK: UICollectionViewDelegateFlowLayout
extension DrawBar: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let device = UIDevice.current.userInterfaceIdiom
        /// Indicates item size to return.
        var size: CGSize = CGSize()
        // Calculating width:
        let width = collectionView.frame.width - (collectionView.contentInset.left + collectionView.contentInset.right)
        
        if device == .pad {
            size.width = width / 14
        }
        else {
            size.width = width / 7
        }
        
        size.height =  size.width

        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}

// - MARK: UIColorPickerViewControllerDelegate
extension DrawBar: UIColorPickerViewControllerDelegate {
    
    func colorPickerViewControllerDidFinish(_ viewController: UIColorPickerViewController) {
        colors.insert(viewController.selectedColor, at: colors.count - 1)
        colorCollection?.reloadData()
    }
    
}
