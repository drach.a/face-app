//
//  DrawBar.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 03.09.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.


import UIKit

/// Indicates draw bar.
class DrawBar: UIView {
    
    // - MARK: Properties
    
    @IBOutlet weak var brushButton: UIButton!
    @IBOutlet weak var eraseButton: UIButton!
    @IBOutlet weak var colorCollection: UICollectionView!
    @IBOutlet weak var brush1: UIButton!
    @IBOutlet weak var brush2: UIButton!
    @IBOutlet weak var brush3: UIButton!
    @IBOutlet weak var brush4: UIButton!
    @IBOutlet weak var brush5: UIButton!
    @IBOutlet weak var brush6: UIButton!
    
    weak var delegate: DrawBarDelegate?
    /// The bar color picker.
    let colorPicker = UIColorPickerViewController()
    var colors: [UIColor] = [#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), #colorLiteral(red: 0.9220944047, green: 0.3408394158, blue: 0.3441366553, alpha: 1), #colorLiteral(red: 0.95156461, green: 0.599634707, blue: 0.2884348631, alpha: 1), #colorLiteral(red: 0.9474667907, green: 0.7861401439, blue: 0.2961711287, alpha: 1), #colorLiteral(red: 0, green: 0.4144335985, blue: 0.1767787635, alpha: 1), #colorLiteral(red: 0.1280970275, green: 0.5889706612, blue: 0.3276189268, alpha: 1), #colorLiteral(red: 0.1499820054, green: 0.6804082394, blue: 0.3783406019, alpha: 1), #colorLiteral(red: 0.10066659, green: 0.6687210798, blue: 0.857529223, alpha: 1), #colorLiteral(red: 0.3397654295, green: 0.7983145714, blue: 0.9496322274, alpha: 1), #colorLiteral(red: 0.1830454767, green: 0.5009093881, blue: 0.9273914695, alpha: 1), #colorLiteral(red: 0, green: 0.3707507849, blue: 0.8658934832, alpha: 1), #colorLiteral(red: 0.6066625118, green: 0.3186083436, blue: 0.8762715459, alpha: 1), #colorLiteral(red: 0.7327192426, green: 0.4201564193, blue: 0.8511666656, alpha: 1), #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)]
    /// Indicates current selected brush.
    private var previousBrush = UIButton()
    
    // - MARK: Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setProperties()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setProperties()
    }
    
    
    // - MARK: Actions
    
    func addTo(view: UIView) {
        view.addSubview(self)
        topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
    }
    
    // - MARK: Private Actions
    
    private func setProperties() {
        translatesAutoresizingMaskIntoConstraints = false
        colorPicker.delegate = self
        colorPicker.isModalInPresentation = true
        colorPicker.modalPresentationStyle = .fullScreen
        loadFromNib(name: "DrawBar")
        previousBrush = brush1
        eraseButton.setImage(UIImage(named: "eraser")?.withTintColor(.label), for: .normal)
        brush1.setImage(UIImage(named: "brush1")?.withTintColor(#colorLiteral(red: 0.1180937961, green: 0.788026154, blue: 0.8793967366, alpha: 1)), for: .normal)
        brush2.setImage(UIImage(named: "brush2")?.withTintColor(.label), for: .normal)
        brush3.setImage(UIImage(named: "brush3")?.withTintColor(.label), for: .normal)
        brush4.setImage(UIImage(named: "brush4")?.withTintColor(.label), for: .normal)
        brush5.setImage(UIImage(named: "brush5")?.withTintColor(.label), for: .normal)
        brush6.setImage(UIImage(named: "brush6")?.withTintColor(.label), for: .normal)
        colorCollection?.register(UINib(nibName:"DrawColorCell", bundle: nil), forCellWithReuseIdentifier: "DrawColorCell")
        colorCollection?.dataSource = self
        colorCollection?.delegate = self
    }
    
    /// Selects draw style brush or erase.
    @IBAction private func selectStyle(_ sender: UIButton) {
        switch sender {
        case brushButton:
            eraseButton.setImage(UIImage(named: "eraser")?.withTintColor(.label), for: .normal)
            brushButton.setImage(UIImage(named: "brushSelected"), for: .normal)
            delegate?.drawBar(self, isDrawing: true)
        case eraseButton:
            brushButton.setImage(UIImage(named: "brush")?.withTintColor(.label), for: .normal)
            eraseButton.setImage(UIImage(named: "eraserSelected"), for: .normal)
            delegate?.drawBar(self, isDrawing: false)
        default:
            return
        }
    }
    
    /// Selects brush  tool size.
    @IBAction private func selectBrushSize(_ sender: UIButton) {
        var brushSize: CGFloat = 3
        selectBrush(sender)
        switch sender {
        case brush1:
            brushSize = 3
        case brush2:
            brushSize = 5
        case brush3:
            brushSize = 8
        case brush4:
            brushSize = 14
        case brush5:
            brushSize = 20
        case brush6:
            brushSize = 30
        default:
            return
        }
        delegate?.drawBar(self, didSelectSize: brushSize)
    }
    
    /// Selects a brush image style.
    private func selectBrush(_ brush: UIButton) {
        previousBrush.setImage(previousBrush.image(for: .normal)?.withTintColor(.label), for: .normal)
        let image = brush.image(for: .normal)?.withTintColor(#colorLiteral(red: 0.1180937961, green: 0.788026154, blue: 0.8793967366, alpha: 1))
        brush.setImage(image, for: .normal)
        previousBrush = brush
    }
    
}
