//
//  DrawBarDelegate.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 03.09.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import UIKit

/// Delegates the DrawBar actions.
protocol DrawBarDelegate: AnyObject {
    

    /// Tells about: the DrawBar selected draw size.
    /// - Parameters:
    ///     - drawBar: Indicates  the DrawBar  object.
    ///     - drawSize: Indicates size of the bar drawing brush..
    func drawBar(_ drawBar: DrawBar, didSelectSize drawSize: CGFloat)
    
    /// Tells about: the DrawBar selected draw color.
    /// - Parameters:
    ///     - drawBar: Indicates  the DrawBar  object.
    ///     - drawColor: Indicates size of the bar drawing color.
    func drawBar(_ drawBar: DrawBar, didPickColor drawColor: UIColor)
    
    /// Tells about: the DrawBar selected draw color.
    /// - Parameters:
    ///     - drawBar: Indicates  the DrawBar  object.
    ///     - isDrawing: Indicates drawing status.
    func drawBar(_ drawBar: DrawBar, isDrawing: Bool)
    
}
