//
//  BlurBarDelegate.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 04.09.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import UIKit

/// Delegates the BlurBar actions.
protocol BlurBarDelegate: AnyObject {
    
    /// Tells about: the BlurBar selected draw size.
    /// - Parameters:
    ///     - blurBar: Indicates  the BlurBar  object.
    ///     - blurSize: Indicates size of the blurBar drawing brush..
    func blurBar(_ blurBar: BlurBar, didSelectSize blurSize: CGFloat)
    
    /// Tells about: the BlurBar chaned blur style.
    /// - Parameters:
    ///     - blurBar: Indicates  the BlurBar  object.
    ///     - didChange: Indicates the BlurBar change style.
    func blurBar(_ blurBar: BlurBar, didChangeStyle style: BlurBar.Style)
    
}
