//
//  BlurField+Extension.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 04.09.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import UIKit

// - MARK: BlurBarDelegate
extension BlurField: BlurBarDelegate {
    
    // When size selected
    func blurBar(_ blurBar: BlurBar, didSelectSize blurSize: CGFloat) {
        drawSize = blurSize
    }
    // When style is selected.
    func blurBar(_ blurBar: BlurBar, didChangeStyle style: BlurBar.Style) {
        switch style {
        case .draw:
            state = .draw
        case .erase:
            state = .erase
        }
    }
    
}
