//
//  BlurBar.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 04.09.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import UIKit

/// Indicates blur bar view.
class BlurBar: UIView {
    
    // Indicates the Bar style
    enum Style {
        case erase
        case draw
    }

    // - MARK: Properties
    
    @IBOutlet weak var brushOutlet: UIButton!
    @IBOutlet weak var eraseOutlet: UIButton!
    @IBOutlet weak var size1: UIButton!
    @IBOutlet weak var size2: UIButton!
    @IBOutlet weak var size3: UIButton!
    @IBOutlet weak var size4: UIButton!
    @IBOutlet weak var size5: UIButton!
    @IBOutlet weak var size6: UIButton!
    
    /// Indicates  previous selected blur brush.
    private var previousBlur = UIButton()
    /// The BlurBar delegates instance.
    weak var delegate: BlurBarDelegate?
    /// Indicates current blur style.
    var style: Style = .draw
    
    // - MARK: Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setProperties()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setProperties()
    }
    
    // - MARK: Actions
    
    /// Adds view blur view to it's superview.
    func addTo(view: UIView) {
        view.addSubview(self)
        topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
    }
    
    // - MARK: Private Actions
    
    private func setProperties() {
        translatesAutoresizingMaskIntoConstraints = false
        loadFromNib(name: "BlurBar")
        previousBlur = size1
        eraseOutlet.setImage(UIImage(named: "eraser")?.withTintColor(.label), for: .normal)
        size1.setImage(UIImage(named: "brush1")?.withTintColor(#colorLiteral(red: 0.1180937961, green: 0.788026154, blue: 0.8793967366, alpha: 1)), for: .normal)
        size2.setImage(UIImage(named: "brush2")?.withTintColor(.label), for: .normal)
        size3.setImage(UIImage(named: "brush3")?.withTintColor(.label), for: .normal)
        size4.setImage(UIImage(named: "brush4")?.withTintColor(.label), for: .normal)
        size5.setImage(UIImage(named: "brush5")?.withTintColor(.label), for: .normal)
        size6.setImage(UIImage(named: "brush6")?.withTintColor(.label), for: .normal)
    }
    
    
    /// Selects draw style brush or erase.
    @IBAction private func selectStyle(_ sender: UIButton) {
        
        switch sender {
        case brushOutlet:
            eraseOutlet.setImage(UIImage(named: "eraser")?.withTintColor(.label), for: .normal)
            brushOutlet.setImage(UIImage(named: "selectedBlur"), for: .normal)
            style = .draw
            
        case eraseOutlet:
            brushOutlet.setImage(UIImage(named: "deselectedBlur")?.withTintColor(.label), for: .normal)
            eraseOutlet.setImage(UIImage(named: "eraserSelected"), for: .normal)
            style = .erase
        default:
           return
        }
        delegate?.blurBar(self, didChangeStyle: style)
    }
    
    /// Selects blur brush  tool size.
    @IBAction private func selectSize(_ sender: UIButton) {
        
        var blurSize: CGFloat = 5
        changeToolImage(sender)
        switch sender {
        case size1:
            blurSize = 5
        case size2:
            blurSize = 8
        case size3:
            blurSize = 10
        case size4:
            blurSize = 16
        case size5:
            blurSize = 22
        case size6:
            blurSize = 32
        default:
           return
        }
        delegate?.blurBar(self, didSelectSize: blurSize)
    }
    
    /// Changes image style on the selected and deselected blur tool.
    private func changeToolImage(_ brush: UIButton) {
        previousBlur.setImage(previousBlur.image(for: .normal)?.withTintColor(.label), for: .normal)
        let image = brush.image(for: .normal)?.withTintColor(#colorLiteral(red: 0.1180937961, green: 0.788026154, blue: 0.8793967366, alpha: 1))
        brush.setImage(image, for: .normal)
        previousBlur = brush
        }
    

}
