//
//  BlurField.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 10.08.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import CoreGraphics
import UIKit

/// Indicates a filed to draw in.
class BlurField: UIImageView {
    
    /// Indicates the view state.
    enum State {
        case draw
        case erase
    }
    
    // - MARK: Properties
    
    /// Indicates current view state.
    var state: State = .draw
    /// Indicates draw color.
    var drawColor: UIColor = .white
    /// Indicates draw width.
    var drawSize: CGFloat = 5
    /// Blur bar
    let blurBar = BlurBar()
    
    // - MARK: Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setProperties()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setProperties()
    }
    convenience init() {
        self.init(frame: .zero)
    }
    
    // - MARK: Actions
    
    /// Adds the draw fiels toit's superview.
    func addTo(view: UIView) {
        view.addSubview(self)
        topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
    }
    
    // - MARK: Private Actions
    
    /// Sets properties of the view.
    private func setProperties() {
        translatesAutoresizingMaskIntoConstraints = false
        isUserInteractionEnabled = true
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panGesture(_:)))
        addGestureRecognizer(panGesture)
        contentMode = .scaleAspectFit
        backgroundColor = .clear
        isOpaque = true
        blurBar.delegate = self
    }
    
    /// Pan gesture action target.
    @objc private func panGesture(_ sender: UIPanGestureRecognizer) {
        
        let currentPoint = sender.location(in: self)
        
        drawColor = superview!.colorFromPoint(currentPoint)
        
        UIGraphicsBeginImageContextWithOptions(frame.size, false, 0)
        let context = UIGraphicsGetCurrentContext()
        context?.setLineWidth(drawSize)
        context?.setLineCap(.round)
        sender.view?.layer.render(in: context!)
        
        switch state {
        
        case .draw:
            context?.setStrokeColor(drawColor.cgColor)
            context?.setAlpha(0.20)
            
        case.erase:
            context?.setBlendMode(.clear)
        }
        
        context?.move(to: currentPoint)
        context?.addLine(to: currentPoint)
        context?.strokePath()
        
        image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
    }
    
}
