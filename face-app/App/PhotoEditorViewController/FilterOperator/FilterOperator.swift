//
//  FilterOperator.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 04.09.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import CoreImage
import UIKit

/// Does operation with a photo.
class FilterOperator {
    
    // - MARK: Filters
    
    /// Indicates current filter  group.
    var filterGroup: FilterGroup = .none
    /// Indicates filter group.
    enum FilterGroup {
        case effects
        case lighting
        case color
        case vignette
        case none
    }
    
    // - MARK: Filter Options.
    
    // Indicate current effect.
    var effect: Effect = .none
    /// Effect filter option.
    enum Effect: String, Equatable, CaseIterable{
        case sepia = "Sepia"
        case bloom = "Bloom"
        case chrome = "Chrome"
        case vibrance = "Vibrance"
        case hue = "Hue"
        case gamma = "Gamma"
        case exposure = "Exposure"
        case zoomBlur = "ZoomBlur"
        case scale = "Scale"
        case flight = "Flight"
        case none
    }
    
    var lighting: Lighting = .none
    /// Lighting filter options, some cases has the same filter raw value key there is not need to add the same raw value.
    enum Lighting: Equatable, CaseIterable {
        case brightness
        case contrast
        case highlights
        case shadows
        case none
    }
    
    var color: Color = .none
    /// Lighting filter options.
    enum Color: Equatable, CaseIterable {
        case saturation
        case warmth
        case tint
        case fade
        case none
    }
    
    // - MARK: Properties
    
    /// Indicates original image.
    private let image: UIImage
    // Indicates current filter.
    private var currentFilter: CIFilter
    /// Indicates initialized image context.
    private var context: CIContext
    
    // - MARK: Initialization
    
    init(image: UIImage, filter: CIFilter, in filterGroup: FilterGroup, context: CIContext) {
        self.image = image
        self.currentFilter = filter
        self.filterGroup = filterGroup
        self.context = context
        currentFilter.setValue(CIImage(image: image), forKey: kCIInputImageKey)
    }
    
    // - MARK: Actions
    
    // Set effects value for chosen filter.
    func setEffects(_ value: Float) -> UIImage {
        var filteredImage = UIImage()
        switch effect {
        
        case .sepia:
            filteredImage = sepia(with: value)
            
        case .bloom:
            filteredImage = bloom(with: value)
            
        case .chrome:
            filteredImage = chrome(with: value)
            
        case .vibrance:
            filteredImage = vibrance(with: value)
            
        case .hue:
            filteredImage = hue(with: value)
            
        case .gamma:
            filteredImage = gamma(with: value)
            
        case .exposure:
            filteredImage = exposure(with: value)
            
        case .zoomBlur:
            filteredImage = zoomBlur(with: value)
            
        case .scale:
            filteredImage = scale(with: value)
            
        case .flight:
            filteredImage = flight(with: value)
            
        case .none:
            filteredImage = image
        }
        return filteredImage
    }
    
    /// Sets the vignette filter value.
    func setVignette(_ value: Float) -> UIImage {
        currentFilter.setValuesForKeys(["inputRadius" : value, "inputIntensity" : value])
        guard let ciImage =  currentFilter.outputImage,
              let cgImage = context.createCGImage(ciImage, from: ciImage.extent)
        else { return image}
        return UIImage(cgImage: cgImage)
    }
    
    /// Sets lighting filters.
    func setLighting(_ value: Float) -> UIImage {
        var filteredImage = UIImage()
        switch lighting {
        
        case .brightness:
            filteredImage = brightness(with: value)
            
        case .contrast:
            filteredImage = contrast(with: value)
            
        case .highlights:
            filteredImage = hightlite(with: value)
            
        case .shadows:
            filteredImage = shadows(with: value)
            
        case .none:
            filteredImage = image
        }
        return filteredImage
    }
    
    /// Sets color options.
    func setColor(_ value: Float) -> UIImage {
        var filteredImage = UIImage()
        switch color {
        case .saturation:
            filteredImage = saturation(with: value)
            
        case .warmth:
            filteredImage = warmth(with: value)
            
        case .fade:
            filteredImage = fade(with: value)
            
        case .tint:
            filteredImage = tint(with: value)
            
        case .none:
            filteredImage = image
        }
        return filteredImage
    }
    /// Sets fade value.
    private func fade(with value: Float) -> UIImage {
        currentFilter.setValue(CIColor(red: 0, green: 0, blue: 0), forKey: "inputColor")
        currentFilter.setValue(value, forKey: "inputIntensity")
        guard let ciImage =  currentFilter.outputImage,
              let cgImage = context.createCGImage(ciImage, from: ciImage.extent)
        else { return image}
        return UIImage(cgImage: cgImage)
    }
    
    /// Creates warmth filter effect.
    private func warmth(with value: Float) -> UIImage {
        
        currentFilter.setValue(CIVector(x: CGFloat(value + 6500), y: 0), forKey: "inputNeutral")
        currentFilter.setValue(CIVector(x: 6500, y: 0), forKey: "inputTargetNeutral")
        guard let ciImage =  currentFilter.outputImage,
              let cgImage = context.createCGImage(ciImage, from: ciImage.extent)
        else { return image}
        return UIImage(cgImage: cgImage)
    }
    /// Sets tint filter value.
    private func tint(with value: Float) -> UIImage {
        currentFilter.setValue(CIVector(x: 6500, y: CGFloat(value)), forKey: "inputNeutral")
        currentFilter.setValue(CIVector(x: 6500, y: 0), forKey: "inputTargetNeutral")
        guard let ciImage =  currentFilter.outputImage,
              let cgImage = context.createCGImage(ciImage, from: ciImage.extent)
        else { return image}
        return UIImage(cgImage: cgImage)
    }
    
    /// Sets saturation filter value.
    private func saturation(with value: Float) -> UIImage {
        currentFilter.setValue(value, forKey: kCIInputSaturationKey)
        guard let ciImage =  currentFilter.outputImage,
              let cgImage = context.createCGImage(ciImage, from: ciImage.extent)
        else { return image}
        return UIImage(cgImage: cgImage)
    }
    
    /// Sets contrast filter value.
    private func contrast(with value: Float) -> UIImage {
        currentFilter.setValue(value, forKey: kCIInputContrastKey)
        guard let ciImage =  currentFilter.outputImage,
              let cgImage = context.createCGImage(ciImage, from: ciImage.extent)
        else { return image}
        return UIImage(cgImage: cgImage)
    }
    /// Sets brightness filter value.
    private func brightness(with value: Float) -> UIImage {
        currentFilter.setValue(value, forKey: kCIInputBrightnessKey)
        guard let ciImage =  currentFilter.outputImage,
              let cgImage = context.createCGImage(ciImage, from: ciImage.extent)
        else { return image}
        return UIImage(cgImage: cgImage)
    }
    
    /// Sets hightlite filter value.
    private func hightlite(with value: Float) -> UIImage {
        currentFilter.setValue(value, forKey: "inputShadowAmount")
        guard let ciImage =  currentFilter.outputImage,
              let cgImage = context.createCGImage(ciImage, from: ciImage.extent)
        else { return image}
        return UIImage(cgImage: cgImage)
    }
    /// Sets shadows filter value.
    private func shadows(with value: Float) -> UIImage {
        currentFilter.setValue(-value, forKey: "inputShadowAmount")
        guard let ciImage =  currentFilter.outputImage,
              let cgImage = context.createCGImage(ciImage, from: ciImage.extent)
        else { return image}
       
        return UIImage(cgImage: cgImage)
    }
    
    // - MARK: Effect Filters
    
    /// Sets sepia filter value.
    private func sepia(with value: Float) -> UIImage {
        currentFilter.setValue(value, forKey: "inputIntensity")
        guard let ciImage =  currentFilter.outputImage,
              let cgImage = context.createCGImage(ciImage, from: ciImage.extent)
        else { return image}
        return UIImage(cgImage: cgImage)
    }
    
    // Sets posterize filter value.
    private func bloom(with value: Float) -> UIImage {
        currentFilter.setValue(50, forKey: kCIInputRadiusKey)
        currentFilter.setValue(value, forKey: kCIInputIntensityKey)
        guard let ciImage =  currentFilter.outputImage,
              let cgImage = context.createCGImage(ciImage, from: CIImage(image: image)!.extent)
        else { return image}
        return UIImage(cgImage: cgImage)
    }
    
    // Sets chrome filter value.
    private func chrome(with value: Float) -> UIImage {
        currentFilter.setValue(CIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 1.0), forKey: "inputColor")
        currentFilter.setValue(value, forKey: "inputIntensity")
        guard let ciImage =  currentFilter.outputImage,
              let cgImage = context.createCGImage(ciImage, from: ciImage.extent)
        else { return image}
        return UIImage(cgImage: cgImage)
    }
    
    /// Sets vibrance filter value.
    private func vibrance(with value: Float) -> UIImage {
        currentFilter.setValue(value, forKey: "inputAmount")
        guard let ciImage =  currentFilter.outputImage,
              let cgImage = context.createCGImage(ciImage, from: ciImage.extent)
        else { return image}
        return UIImage(cgImage: cgImage)
    }
    
    /// Sets hue filter value.
    private func hue(with value: Float) -> UIImage {
        currentFilter.setValue(value, forKey: "inputAngle")
        guard let ciImage =  currentFilter.outputImage,
              let cgImage = context.createCGImage(ciImage, from: ciImage.extent)
        else { return image}
        return UIImage(cgImage: cgImage)
    }
    
    /// Sets gamma filter value.
    private func gamma(with value: Float) -> UIImage {
        currentFilter.setValue(value, forKey: "inputPower")
        guard let ciImage =  currentFilter.outputImage,
              let cgImage = context.createCGImage(ciImage, from: ciImage.extent)
        else { return image}
        return UIImage(cgImage: cgImage)
    }
    
    /// Sets exposure filter value.
    private func exposure(with value: Float) -> UIImage {
        currentFilter.setValue(value, forKey: "inputEV")
        guard let ciImage =  currentFilter.outputImage,
              let cgImage = context.createCGImage(ciImage, from: ciImage.extent)
        else { return image}
        return UIImage(cgImage: cgImage)
    }
    
    /// Sets zoomBlur filter value.
    private func zoomBlur(with value: Float) -> UIImage {
        currentFilter.setValue(value, forKey: "inputRadius")
        guard let ciImage =  currentFilter.outputImage,
              let cgImage = context.createCGImage(ciImage, from: ciImage.extent)
        else { return image}
        return UIImage(cgImage: cgImage)
    }
    
    /// Sets scale filter value.
    private func scale(with value: Float) -> UIImage {
        currentFilter.setValue(value, forKey: "inputScale")
        currentFilter.setValue(value, forKey: "inputAspectRatio")
        guard let ciImage =  currentFilter.outputImage,
              let cgImage = context.createCGImage(ciImage, from: ciImage.extent)
        else { return image}
        return UIImage(cgImage: cgImage)
    }
    
    /// Sets flight filter value.
    private func flight(with value: Float) -> UIImage {
        currentFilter.setValue(value, forKey: "inputAngle")
        guard let ciImage =  currentFilter.outputImage,
              let cgImage = context.createCGImage(ciImage, from: ciImage.extent)
        else { return image}
        return UIImage(cgImage: cgImage)
    }
    
}
