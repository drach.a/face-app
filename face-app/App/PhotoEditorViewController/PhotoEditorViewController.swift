//
//  PhotoEditorViewController.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 02.07.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import UIKit
import CoreImage

/// Editing view controller.
class PhotoEditorViewController: UIViewController {
    
    // - MARK: Properties
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var editorNavBar: EditorNavigationBar!
    @IBOutlet weak var editorTabBar: EditorTabBar!
    
    
    /// Indicates editing ccontext.
    var context = CIContext()
    /// Indicates blur area.
    var blurField: BlurField?
    /// Indicates draw area.
    var drawField: DrawField?
    /// Indicates fresh loaded image.
    var freshImage: UIImage!
    /// Indicates not filtered image.
    var notFilteredImage: UIImage?
    /// Indicates the text option label.
    var textLabel: TextLabel?
    /// Indicates array of texLabels.
    private var textLabels: [TextLabel] = []
    /// Indicates add text label button.
    var textButton: AddText?
    /// Indicates filters slider.
    var slider: Slider?
    
    /// Indicates cropview frame.
    var cropView: EditorPhotoCropper?
    
    /// Indicates editing photo model.
    var photoModel: EditingPhotoModel?
    /// Indicates editing photo filter operator.
    var filterOperator: FilterOperator?
    /// Indicates delegate instance.
    weak var delegate: EditorControllerDelegate?
    
   
    
    
    // - MARK: Controller LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        modalTransitionStyle = .flipHorizontal
        delegate?.editorController(self, didLoadView: true)
        editorTabBar.editorDelegate = self
        setNotificationObservers()
        prepareViewForEditing()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        editorNavBar.setShadows()
        editorTabBar.setShadows()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        createNails()
    }
    
    // - MARK: Actions
    
    func createNails() {
        let resizedImage = freshImage.resized(with: 400.0)
        editorTabBar.effectsImages.append(resizedImage)
        filterOperator = FilterOperator(image: resizedImage, filter: CIFilter(name: "CISepiaTone")!, in: .effects, context: context)
        filterOperator?.effect = .sepia
        editorTabBar.effectsImages.append(filterOperator!.setEffects(1.0))
        filterOperator = FilterOperator(image: resizedImage, filter: CIFilter(name: "CIBloom")!, in: .effects, context: context)
        filterOperator?.effect = .bloom
        editorTabBar.effectsImages.append(filterOperator!.setEffects(1.0))
        filterOperator = FilterOperator(image: resizedImage, filter: CIFilter(name: "CIColorMonochrome")!, in: .effects, context: context)
        filterOperator?.effect = .chrome
        editorTabBar.effectsImages.append(filterOperator!.setEffects(1.0))
        filterOperator = FilterOperator(image: resizedImage, filter: CIFilter(name: "CIVibrance")!, in: .effects, context: context)
        filterOperator?.effect = .vibrance
        editorTabBar.effectsImages.append(filterOperator!.setEffects(5.0))
        filterOperator = FilterOperator(image: resizedImage, filter: CIFilter(name: "CIHueAdjust")!, in: .effects, context: context)
        filterOperator?.effect = .hue
        editorTabBar.effectsImages.append(filterOperator!.setEffects(3.0))
        filterOperator = FilterOperator(image: resizedImage, filter: CIFilter(name: "CIGammaAdjust")!, in: .effects, context: context)
        filterOperator?.effect = .gamma
        editorTabBar.effectsImages.append(filterOperator!.setEffects(1.6))
        filterOperator = FilterOperator(image: resizedImage, filter: CIFilter(name: "CIExposureAdjust")!, in: .effects, context: context)
        filterOperator?.effect = .exposure
        editorTabBar.effectsImages.append(filterOperator!.setEffects(3.0))
        filterOperator = FilterOperator(image: resizedImage, filter: CIFilter(name: "CIGaussianBlur")!, in: .effects, context: context)
        filterOperator?.effect = .zoomBlur
        editorTabBar.effectsImages.append(filterOperator!.setEffects(10.0))
        filterOperator = FilterOperator(image: resizedImage, filter: CIFilter(name: "CILanczosScaleTransform")!, in: .effects, context: context)
        filterOperator?.effect = .scale
        editorTabBar.effectsImages.append(filterOperator!.setEffects(0.5))
        filterOperator = FilterOperator(image: resizedImage, filter: CIFilter(name: "CIStraightenFilter")!, in: .effects, context: context)
        filterOperator?.effect = .flight
        editorTabBar.effectsImages.append(filterOperator!.setEffects(4.0))
        filterOperator = nil
    }
    
    func prepareViewForEditing() {
        guard let image = photoModel?.editedImage else { return }
        freshImage = image
        imageView.image = image
        notFilteredImage = image
    }
    
    /// Applys changes to image.
    func applyChanges() {
        freshImage = imageView.image
    }
    
    
    // - MARK: Private Actions
    
    /// Sets the NotificationCenter observers to listen for some events.
    private func setNotificationObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(addToolBar), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(finishEditing), name: Notification.Name("editorDone"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(cancelEditing), name: Notification.Name("editorCancel"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(addText), name: Notification.Name("addText"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(presentColorPicker), name: Notification.Name("presentColorPicker"), object: nil)
    }
    
    // - MARK: NotificationCenter Targets
    
    /// Adds tollbar when keyboard appear.
    @objc private func addToolBar(notification: NSNotification) {
        if textLabel?.textBar == nil {
            textLabel?.textBar = TextLabelBar()
            textLabel?.textBar?.delegate = textLabel
            guard let keyboardRect = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
            textLabel?.textBar?.addTo(view: view, with: keyboardRect.size)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                self.textLabel?.textBar?.isHidden = false
            }
        }
    }
    
    @objc private func finishEditing() {
        
        switch editorNavBar.state {
        
        case .standart:
            photoModel?.editedImage = freshImage
            delegate?.editorController(self, didFinishEditing: true)
            dismiss(animated: true)
            
        case .text:
            editorNavBar.setTitle(.standart)
            editorTabBar.setDefaultConfiguration()
            finishTextEditing(saving: true)
            
        case .draw:
            editorNavBar.setTitle(.standart)
            editorTabBar.setDefaultConfiguration()
            finishDraw(saving: true)
            
        case .effects:
            editorNavBar.setTitle(.standart)
            editorTabBar.setDefaultConfiguration()
            removeSlider()
            applyChanges()
            
        case .orientation:
            editorNavBar.setTitle(.standart)
            editorTabBar.setDefaultConfiguration()
            applyChanges()
            
        case .crop:
            editorNavBar.setTitle(.standart)
            editorTabBar.setDefaultConfiguration()
            if cropView?.cropAspect == .custom {
                imageView.image = cropView?.crop()
            }
            applyChanges()
            
        case .lighting:
            editorNavBar.setTitle(.standart)
            editorTabBar.setDefaultConfiguration()
            removeSlider()
            applyChanges()
            
        case .color:
            editorNavBar.setTitle(.standart)
            editorTabBar.setDefaultConfiguration()
            removeSlider()
            applyChanges()
            
        case .vignette:
            editorNavBar.setTitle(.standart)
            editorTabBar.setDefaultConfiguration()
            removeSlider()
            applyChanges()
            
        case .blur:
            editorNavBar.setTitle(.standart)
            editorTabBar.setDefaultConfiguration()
            finishBlur(saving: true)
        }
        
       
    }
    
    @objc private func presentColorPicker() {
        if let label = textLabel,
           let bar = label.textBar {
            present(bar.colorPicker, animated: true)
        }
        else if let drawField = self.drawField {
            present( drawField.drawBar.colorPicker, animated: true)
        }
    }
    
    // Cancel editing without saving.
    @objc private func cancelEditing() {
        switch editorNavBar.state {
        
        case .standart:
            photoModel?.setOriginal()
            delegate?.editorController(self, didFinishEditing: true)
            dismiss(animated: true)
            
        case .text:
            editorNavBar.setTitle(.standart)
            editorTabBar.setDefaultConfiguration()
            finishTextEditing(saving: false)
            
        case .draw:
            editorNavBar.setTitle(.standart)
            editorTabBar.setDefaultConfiguration()
            finishDraw(saving: false)
            
        case .effects:
            editorNavBar.setTitle(.standart)
            editorTabBar.setDefaultConfiguration()
            removeSlider()
            imageView.image = notFilteredImage
            freshImage = notFilteredImage
            
        case .orientation:
            editorNavBar.setTitle(.standart)
            editorTabBar.setDefaultConfiguration()
            imageView.image = freshImage
            
        case .crop:
            editorNavBar.setTitle(.standart)
            editorTabBar.setDefaultConfiguration()
            imageView.image = freshImage
            cropView = nil
            
        case .lighting:
            editorNavBar.setTitle(.standart)
            editorTabBar.setDefaultConfiguration()
            removeSlider()
            imageView.image = notFilteredImage
            freshImage = notFilteredImage
            
        case .color:
            editorNavBar.setTitle(.standart)
            editorTabBar.setDefaultConfiguration()
            removeSlider()
            imageView.image = notFilteredImage
            freshImage = notFilteredImage
            
        case .vignette:
            editorNavBar.setTitle(.standart)
            editorTabBar.setDefaultConfiguration()
            removeSlider()
            imageView.image = notFilteredImage
            freshImage = notFilteredImage
            
        case .blur:
            editorNavBar.setTitle(.standart)
            editorTabBar.setDefaultConfiguration()
            finishBlur(saving: false)
        }
    }
    
    @objc private func addText() {
        textLabel = TextLabel()
        textLabels.append(textLabel!)
        textLabel?.set(in: imageView)
        textLabel?.label.becomeFirstResponder()
    }
    
    // - MARK: Slider Operations.
    
    
    func addSlider(standart: Bool) {
        if standart {
            slider?.removeFromSuperview()
            slider = Slider()
            slider?.set(in: view, slider: .standart)
            slider?.delegate = self
        }
        else if !standart {
            slider?.removeFromSuperview()
            editorTabBar.tabBarCollection.isHidden = true
            slider = Slider()
            slider?.set(in: editorTabBar, slider: .vignette)
            slider?.delegate = self
        }
    }
    
    func getSliderValue() {
        if slider != nil {
            switch slider?.valueState {
            case .sepia:
                photoModel?.sepia = slider!.currentValue
            case .bloom:
                photoModel?.bloom = slider!.currentValue
            case .chrome:
                photoModel?.chrome = slider!.currentValue
            case .vibrance:
                photoModel?.vibrance = slider!.currentValue
            case .hue:
                photoModel?.hue = slider!.currentValue
            case .gamma:
                photoModel?.gamma = slider!.currentValue
            case .exposure:
                photoModel?.exposure = slider!.currentValue
            case .zoomBlur:
                photoModel?.zoomBlur = slider!.currentValue
            case .scale:
                photoModel?.scale = slider!.currentValue
            case .flight:
                photoModel?.flight = slider!.currentValue
            case .brightness:
                photoModel?.brightness = slider!.currentValue
            case .contrast:
                photoModel?.contrast = slider!.currentValue
            case .highlights:
                photoModel?.highlights = slider!.currentValue
            case .shadows:
                photoModel?.shadows = slider!.currentValue
            case .saturation:
                photoModel?.saturation = slider!.currentValue
            case .warmth:
                photoModel?.warmth = slider!.currentValue
            case .fade:
                photoModel?.fade = slider!.currentValue
            case .tint:
                photoModel?.tint = slider!.currentValue
            default:
                return
            }
            }
    }
    
    /// Removes slider
    func removeSlider() {
        self.slider?.removeFromSuperview()
        self.slider = nil
        editorTabBar.tabBarCollection.isHidden = false
    }
    
    // - MARK: Crop Operations
   
    func cropImageWith(aspect: EditorPhotoCropper.CropAspect) {
        cropView?.removeFromSuperview()
        imageView.image = freshImage
        cropView = EditorPhotoCropper()
        cropView?.addTo(view: containerView, with: imageView.imageFrame())
        cropView?.setCropper(with: aspect)
        cropView?.image = freshImage
        imageView.image = cropView?.crop()
    }
    
   // - MARK: Text Operations.
    func setText() {
        editorTabBar.tabBarCollection.isHidden = true
        textButton = AddText()
        textButton?.addTo(view: editorTabBar)
        addText()
    }
    
    
    /// Finishes textEditing.
    func finishTextEditing(saving: Bool) {
        textButton?.removeFromSuperview()
        // remove from array
        for label in textLabels {
            label.finishEditing()
        }
        if saving {
            imageView.image = imageView.takeoutImage()
            freshImage = imageView.image
            notFilteredImage = imageView.image
        }
        for subView in imageView.subviews {
            subView.removeFromSuperview()
        }
        textLabel = nil
        textButton = nil
        editorTabBar.tabBarCollection.isHidden = false
    }
    
    // - MARK: Draw Operations
    
    func setDaw() {
        drawField = DrawField()
        drawField?.addTo(view: imageView)
        editorTabBar.tabBarCollection.isHidden = true
        drawField?.drawBar.addTo(view: editorTabBar)
    }
    
    func finishDraw(saving: Bool) {
        if saving {
        imageView.image = imageView.takeoutImage()
        freshImage = imageView.image
        notFilteredImage = imageView.image
        }
        drawField?.removeFromSuperview()
        drawField?.drawBar.removeFromSuperview()
        drawField = nil
        editorTabBar.tabBarCollection.isHidden = false
    }
    
    // - MARK: Blur Operations
    
    func setBlur() {
        blurField = BlurField()
        blurField?.addTo(view: imageView)
        editorTabBar.tabBarCollection.isHidden = true
        blurField?.blurBar.addTo(view: editorTabBar)
    }
    
    func finishBlur(saving: Bool) {
        if saving {
        imageView.image = imageView.takeoutImage()
        freshImage = imageView.image
        notFilteredImage = imageView.image
        }
        blurField?.removeFromSuperview()
        blurField?.blurBar.removeFromSuperview()
        blurField = nil
        editorTabBar.tabBarCollection.isHidden = false
    }
    
    
    
}
