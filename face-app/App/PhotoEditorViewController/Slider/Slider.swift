//
//  Slider.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 04.08.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import UIKit

// Slider view.
class Slider: UIView {
    
    
    // - MARK: Stucts
    enum Value {
        case sepia
        case bloom
        case chrome
        case vibrance
        case hue
        case gamma
        case exposure
        case zoomBlur
        case scale
        case flight
        case brightness
        case contrast
        case highlights
        case shadows
        case saturation
        case warmth
        case fade
        case tint
        case none
    }
    
    /// Indicates slider state.
    enum State: CaseIterable, Equatable {
        case standart
        case vignette
    }
    
    // - MARK: Properties
    
    /// Value state.
    var valueState: Value = .none
    
    @IBOutlet weak var slider: UISlider!
    
    /// Indicates current value
    var currentValue: Float {
        return slider.value
    }
    /// Indicates the slider state.
    var state: State = .standart
    /// The slide rdelegate.
    weak var delegate: SliderDelegate?
    
    // - MARK: Initialization
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        loadFromNib(name: "Slider")
        backgroundColor = .clear
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadFromNib(name: "Slider")
        backgroundColor = .clear
    }
    
    // - MARK: Actions
    
    /// Sets slider in it's superview.
    func set(in view: UIView, slider: State) {
        switch slider {
        
        case .standart:
            let width: CGFloat = 300
            let height: CGFloat = 40
            let x = view.center.x - (width / 2)
            let y = view.center.y + view.frame.height / 4
            frame = CGRect(x: x, y: y, width: width, height: height)
            
        case .vignette:
            let width: CGFloat = 350
            let height: CGFloat = 60
            let x = view.frame.width / 2 - (width / 2)
            let y = -view.frame.height + (height * 2)
            frame = CGRect(x: x, y: y, width: width, height: height)
        }
        DispatchQueue.main.async {
            view.addSubview(self)
        }
    }
    
    
    /// Sets slider value.
    func set(value: Float, min: Float, max: Float, for state: Value) {
        self.valueState = state
        slider.value = value
            slider.minimumValue = min
            slider.maximumValue = max
       
    }
    

    // - MARK: Private Actions
    
    @IBAction private func changeValueAction(_ sender: UISlider) {
        delegate?.slider(self, didChangeValue: sender.value)
    }
    
}
