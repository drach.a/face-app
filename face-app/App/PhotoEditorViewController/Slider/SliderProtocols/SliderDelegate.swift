//
//  SliderDelegate.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 18.08.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import Foundation

/// Delegates the Slider actions.
protocol SliderDelegate: AnyObject {
    
    /// Tells about: the  Slider  did changed value.
    /// - Parameters:
    ///     - slider: Indicates the Slider instance.
    ///     - value: Indicates the slider current value..
    func slider(_ slider: Slider, didChangeValue value: Float)
}


