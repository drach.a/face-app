//
//  PhotoEditorViewController+Extension.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 04.08.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import CoreImage
import UIKit



// - MARK: EditorTabBarDelegate
extension PhotoEditorViewController: EditorTabBarDelegate {
    
    // When text option is selected.
    func barEditor(_ barEditor: EditorTabBar, didSelectTextOption textKey: String) {
        editorNavBar.setTitle(.text)
        setText()
    }
    
    // When draw option is selected.
    func barEditor(_ barEditor: EditorTabBar, didSelectDrawOption drawKey: String) {
        editorNavBar.setTitle(.draw)
        setDaw()
    }
    
    // When filter option is selected.
    func barEditor(_ barEditor: EditorTabBar, didSelectEffectsOption effectsKey: String) {
        let effects = OptionReader.Effects(rawValue: effectsKey)
        editorNavBar.setTitle(.effects)
        switch effects {
        
        case .original:
            imageView.image = notFilteredImage
            getSliderValue()
            removeSlider()
            photoModel?.setDefaultEffects()
            
        case .sepia:
            getSliderValue()
            applyChanges()
            filterOperator = FilterOperator(image: freshImage, filter: CIFilter(name: "CISepiaTone")!, in: .effects, context: context)
            filterOperator?.effect = .sepia
            addSlider(standart: true)
            slider?.set(value: photoModel!.sepia, min: 0.0, max: 1.0, for: .sepia)
            
        case .bloom:
            getSliderValue()
            applyChanges()
            filterOperator = FilterOperator(image: freshImage, filter: CIFilter(name: "CIBloom")!, in: .effects, context: context)
            filterOperator?.effect = .bloom
            addSlider(standart: true)
            slider?.set(value: photoModel!.bloom, min: 0.0, max: 1.0, for: .bloom)
            
        case .chrome:
            getSliderValue()
            applyChanges()
            filterOperator = FilterOperator(image: freshImage, filter: CIFilter(name: "CIColorMonochrome")!, in: .effects, context: context)
            filterOperator?.effect = .chrome
            addSlider(standart: true)
            slider?.set(value: photoModel!.chrome, min: 0.0, max: 1.0, for: .chrome)
            
        case .vibrance:
            getSliderValue()
            applyChanges()
            filterOperator = FilterOperator(image: freshImage, filter: CIFilter(name: "CIVibrance")!, in: .effects, context: context)
            filterOperator?.effect = .vibrance
            addSlider(standart: true)
            slider?.set(value: photoModel!.vibrance, min: 0.0, max: 5.0, for: .vibrance)
            
        case .hue:
            getSliderValue()
            applyChanges()
            filterOperator = FilterOperator(image: freshImage, filter: CIFilter(name: "CIHueAdjust")!, in: .effects, context: context)
            filterOperator?.effect = .hue
            addSlider(standart: true)
            slider?.set(value: photoModel!.hue, min: -3.0, max: 3.0, for: .hue)
            
        case .gamma:
            getSliderValue()
            applyChanges()
            filterOperator = FilterOperator(image: freshImage, filter: CIFilter(name: "CIGammaAdjust")!, in: .effects, context: context)
            filterOperator?.effect = .gamma
            addSlider(standart: true)
            slider?.set(value: photoModel!.gamma, min: 0.5, max: 1.5, for: .gamma)
            
        case .exposure:
            getSliderValue()
            applyChanges()
            filterOperator = FilterOperator(image: freshImage, filter: CIFilter(name: "CIExposureAdjust")!, in: .effects, context: context)
            filterOperator?.effect = .exposure
            addSlider(standart: true)
            slider?.set(value: photoModel!.exposure, min: -5.0, max: 5.0, for: .exposure)
            
        case .zoomBlur:
            getSliderValue()
            applyChanges()
            filterOperator = FilterOperator(image: freshImage, filter: CIFilter(name: "CIGaussianBlur")!, in: .effects, context: context)
            filterOperator?.effect = .zoomBlur
            addSlider(standart: true)
            slider?.set(value: photoModel!.zoomBlur, min: 0.0, max: 12.0, for: .zoomBlur)
            
        case .scale:
            getSliderValue()
            applyChanges()
            filterOperator = FilterOperator(image: freshImage, filter: CIFilter(name: "CILanczosScaleTransform")!, in: .effects, context: context)
            filterOperator?.effect = .scale
            addSlider(standart: true)
            slider?.set(value: photoModel!.scale, min: 0.0, max: 2.0, for: .scale)
            
        case .flight:
            getSliderValue()
            applyChanges()
            filterOperator = FilterOperator(image: freshImage, filter: CIFilter(name: "CIStraightenFilter")!, in: .effects, context: context)
            filterOperator?.effect = .flight
            addSlider(standart: true)
            slider?.set(value: photoModel!.flight, min: 0.0, max: 6.3, for: .flight)
            
        case .none:
            return
        }
    }
    
    // When orientation option is selected.
    func barEditor(_ barEditor: EditorTabBar, didSelectOrientationOption orientationKey: String) {
        let orientation = OptionReader.Orientation(rawValue: orientationKey)
        editorNavBar.setTitle(.orientation)
        
        switch orientation {
        case .left:
            imageView.image = imageView.image?.imageRotatedByDegrees(degrees: -90, flip: false)
        case .right:
            imageView.image = imageView.image?.imageRotatedByDegrees(degrees: 90, flip: false)
        case .vertical:
            imageView.image = imageView.image?.imageRotatedByDegrees(degrees: 0, flip: true)
        case .horizontal:
            imageView.image = imageView.image?.imageRotatedByDegrees(degrees: 180, flip: true)
        case .none:
            return
        }
    }
    
    // When crop option is selected.
    func barEditor(_ barEditor: EditorTabBar, didSelectCropOption cropKey: String) {
        let crop = OptionReader.Crop(rawValue: cropKey)
        editorNavBar.setTitle(.crop)
        switch crop {
        
        case .custom:
            barEditor.setSelectedIndex()
            imageView.image = freshImage
            cropView = EditorPhotoCropper()
            cropView?.addTo(view: containerView, with: imageView.imageFrame())
            cropView?.setCropper(with: .custom)
            cropView?.image = freshImage
            
        case .original:
            barEditor.setSelectedIndex()
            imageView.image = freshImage
            cropView?.removeFromSuperview()
        
        case .square:
            barEditor.setSelectedIndex()
            cropImageWith(aspect: .square)
        
        case .four_three:
            barEditor.setSelectedIndex()
            cropImageWith(aspect: .four_three)
        
        case .six_four:
            barEditor.setSelectedIndex()
            cropImageWith(aspect: .six_four)
        
        case .seven_five:
            barEditor.setSelectedIndex()
            cropImageWith(aspect: .seven_five)
        
        case .ten_eight:
            barEditor.setSelectedIndex()
            cropImageWith(aspect: .ten_eight)
        
        case .sixteen_nine:
            barEditor.setSelectedIndex()
            cropImageWith(aspect: .sixteen_nine)
        case .none:
            return
        }
        
    }
    
    // When lighting option is selected.
    func barEditor(_ barEditor: EditorTabBar, didSelectLightingOption lightingKey: String) {
        let lighting = OptionReader.Lighting(rawValue: lightingKey)
        editorNavBar.setTitle(.lighting)
        switch lighting {
        
        case .brightness:
            getSliderValue()
            barEditor.setSelectedIndex()
            applyChanges()
            filterOperator = FilterOperator(image: freshImage, filter: CIFilter(name: "CIColorControls")!, in: .lighting, context: context)
            filterOperator?.lighting = .brightness
            addSlider(standart: true)
            slider?.set(value: photoModel!.brightness, min: 0.0, max: 0.8, for: .brightness)
            
        case .contrast:
            getSliderValue()
            barEditor.setSelectedIndex()
            applyChanges()
            filterOperator = FilterOperator(image: freshImage, filter: CIFilter(name: "CIColorControls")!, in: .lighting, context: context)
            filterOperator?.lighting = .contrast
            addSlider(standart: true)
            slider?.set(value: photoModel!.contrast, min: 1.0, max: 2.0, for: .contrast)
            
        case .highlights:
            getSliderValue()
            barEditor.setSelectedIndex()
            applyChanges()
            filterOperator = FilterOperator(image: freshImage, filter: CIFilter(name: "CIHighlightShadowAdjust")!, in: .lighting, context: context)
            filterOperator?.lighting = .highlights
            addSlider(standart: true)
            slider?.set(value: photoModel!.highlights, min: 0.0, max: 1.0, for: .highlights)
            
        case .shadows:
            getSliderValue()
            barEditor.setSelectedIndex()
            applyChanges()
            filterOperator = FilterOperator(image: freshImage, filter: CIFilter(name: "CIHighlightShadowAdjust")!, in: .lighting, context: context)
            filterOperator?.lighting = .shadows
            addSlider(standart: true)
            slider?.set(value: photoModel!.shadows, min: 0.0, max: 1.0, for: .shadows)
            
        case .none:
            return
        }
    }
    
    // When color option is selected.
    func barEditor(_ barEditor: EditorTabBar, didSelectColorOption colorKey: String) {
        let color = OptionReader.Color(rawValue: colorKey)
        editorNavBar.setTitle(.color)
        switch color {
        
        case .saturation:
            getSliderValue()
            barEditor.setSelectedIndex()
            applyChanges()
            filterOperator = FilterOperator(image: freshImage, filter: CIFilter(name: "CIColorControls")!, in: .color, context: context)
            filterOperator?.color = .saturation
            addSlider(standart: true)
            slider?.set(value: photoModel!.saturation, min: 0.0, max: 2.0, for: .saturation)
            
        case .warmth:
            getSliderValue()
            barEditor.setSelectedIndex()
            applyChanges()
            filterOperator = FilterOperator(image: freshImage, filter: CIFilter(name: "CITemperatureAndTint")!, in: .color, context: context)
            filterOperator?.color = .warmth
            addSlider(standart: true)
            slider?.set(value: photoModel!.warmth, min: -3500, max: 3500, for: .warmth)
            
        case .fade:
            getSliderValue()
            barEditor.setSelectedIndex()
            applyChanges()
            filterOperator = FilterOperator(image: freshImage, filter: CIFilter(name: "CIColorMonochrome")!, in: .color, context: context)
            filterOperator?.color = .fade
            addSlider(standart: true)
            slider?.set(value: photoModel!.fade, min: 0.0, max: 0.5, for: .fade)
            
        case .tint:
            getSliderValue()
            barEditor.setSelectedIndex()
            applyChanges()
            filterOperator = FilterOperator(image: freshImage, filter: CIFilter(name: "CITemperatureAndTint")!, in: .color, context: context)
            filterOperator?.color = .tint
            addSlider(standart: true)
            slider?.set(value: photoModel!.tint, min: -150.0, max: 150.0, for: .tint)
            print("Tint value in photoModel: \(photoModel!.tint)")
            print(String("Tint value in slider: \(slider?.currentValue)"))
        case .none:
            return
        }
    }
    
    // When vignette option is selected.
    func barEditor(_ barEditor: EditorTabBar, didSelectVignetteOption vignetteKey: String) {
        editorNavBar.setTitle(.vignette)
        filterOperator = FilterOperator(image: freshImage, filter: CIFilter(name: "CIVignette")!, in: .vignette, context: context)
        addSlider(standart: false)
        slider?.set(value: 1.0, min: 1.0, max: 30.0, for: .none)
    }
    // When blur option is selected.
    func barEditor(_ barEditor: EditorTabBar, didSelectBlurOption blurKey: String) {
        editorNavBar.setTitle(.blur)
        setBlur()
    }
    
}

// - MARK: SliderDelegate
extension PhotoEditorViewController: SliderDelegate {
    
    func slider(_ slider: Slider, didChangeValue value: Float) {
        
        switch filterOperator?.filterGroup {
        
        case .lighting:
            imageView.image = filterOperator?.setLighting(value)
        case .effects:
            imageView.image = filterOperator?.setEffects(value)
        case .color:
            imageView.image = filterOperator?.setColor(value)
        case .vignette:
            imageView.image = filterOperator?.setVignette(value)
        default:
            return
        }
    }
    
}


