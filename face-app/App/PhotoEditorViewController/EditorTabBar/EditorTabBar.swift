//
//  EditorTabBar.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 29.07.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import UIKit

/// Indicates the PhotoEditorViewController scroll tabBar.
class EditorTabBar: UIView {
    
    // - MARK: Properties
    
    /// Indicates the TabBar CollectionView.
    @IBOutlet weak var tabBarCollection: UICollectionView!
    /// Indicates collectionView selected effect index path.
    lazy var selectedEffectIndex = -1
    /// Indicates the TabBar collection cell id.
    let cellId = "EditCell"
    /// Indicates effect cell id.
    let effectsCell = "EffectCell"
    /// Indicates an array of thumbnail images for effects cell.
    var effectsImages: [UIImage] = []
    /// Indicates device state.
    let device = UIDevice.current.userInterfaceIdiom
    /// Indicates the bar options.
    var barOptions = EditorOptions()
    /// Indicates protocol delegation.
    weak var editorDelegate: EditorTabBarDelegate?
    /// Indicates the tabBarCollection selected item index.
    var selectedIndex: IndexPath?
    
    // - MARK: Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setProperties()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setProperties()
    }
    
    // - MARK: Private Actions
    
    /// Sets properties of the StartTabBar.
    private func setProperties() {
        self.loadFromNib(name: "EditorTabBar")
        tabBarCollection.backgroundColor = .clear
        isUserInteractionEnabled = true
        setDelegates()
    }
    
    /// Sets needed delegates.
    private func setDelegates() {
        tabBarCollection.register(UINib(nibName:"EditorCollectionCell", bundle: nil), forCellWithReuseIdentifier: cellId)
        tabBarCollection.register(UINib(nibName:"EffectCollectionCell", bundle: nil), forCellWithReuseIdentifier: effectsCell)
        tabBarCollection?.dataSource = self
        tabBarCollection?.delegate = self
    }
    
    /// Reload options if the selected option has sub-options.
    /// - Parameters:
    /// - rawValue: Indicates String option representation.
    func reloadOptionsIfNeeded(by rawValue: String) {
        if barOptions.state == .standart {
            barOptions.setState(by: rawValue)
            tabBarCollection.reloadData()
            selectedIndex = nil
        }
    }
    
    /// Sets the EditorTabBar default configuration.
    func setDefaultConfiguration() {
        barOptions.state = .standart
        tabBarCollection.reloadData()
    }
    
    /// Sets the tabBarCollection  index to last selected one, if the index selection was deselected by scrolling.
    func setSelectedIndex() {
        selectedIndex = tabBarCollection.indexPathsForSelectedItems?.first
    }
    
}
