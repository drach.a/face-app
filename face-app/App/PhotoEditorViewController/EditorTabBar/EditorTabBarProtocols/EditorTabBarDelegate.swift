//
//  EditorTabBarDelegate.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 03.08.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import Foundation

/// Delegates the EditorTabBar a selected option actions.
protocol EditorTabBarDelegate: AnyObject {
    
    /// Tells about: EditorTabBar selected Text editing option.
    /// - Parameters:
    ///     - barEditor: Indicates EditorTabBar where options are located.
    ///     - orientationKey: String representation of selected text option key.
    func barEditor(_ barEditor: EditorTabBar, didSelectTextOption textKey: String)
    
    /// Tells about: EditorTabBar selected Draw editing option.
    /// - Parameters:
    ///     - barEditor: Indicates EditorTabBar where options are located.
    ///     - orientationKey: String representation of selected draw option key.
    func barEditor(_ barEditor: EditorTabBar, didSelectDrawOption drawKey: String)
    
    /// Tells about: EditorTabBar selected Effects editing option.
    /// - Parameters:
    ///     - barEditor: Indicates EditorTabBar where options are located.
    ///     - filterKey: String representation of selected effects option key.
    func barEditor(_ barEditor: EditorTabBar, didSelectEffectsOption effectsKey: String)
    
    /// Tells about: EditorTabBar selected Orientation editing option.
    /// - Parameters:
    ///     - barEditor: Indicates EditorTabBar where options are located.
    ///     - orientationKey: String representation of selected orientation option key.
    func barEditor(_ barEditor: EditorTabBar, didSelectOrientationOption orientationKey: String)
    
    /// Tells about: EditorTabBar selected crop editing option.
    /// - Parameters:
    ///     - barEditor: Indicates EditorTabBar where options are located.
    ///     - optionKey: String representation of selected crop option key.
    func barEditor(_ barEditor: EditorTabBar, didSelectCropOption cropKey: String)
    
    /// Tells about: EditorTabBar selected Lighting editing option.
    /// - Parameters:
    ///     - barEditor: Indicates EditorTabBar where options are located.
    ///     - lightingKey: String representation of selected lighting option key.
    func barEditor(_ barEditor: EditorTabBar, didSelectLightingOption lightingKey: String)
    
    /// Tells about: EditorTabBar selected Color editing option.
    /// - Parameters:
    ///     - barEditor: Indicates EditorTabBar where options are located.
    ///     - colorKey: String representation of selected color option key.
    func barEditor(_ barEditor: EditorTabBar, didSelectColorOption colorKey: String)
    
    /// Tells about: EditorTabBar selected Vignette editing option.
    /// - Parameters:
    ///     - barEditor: Indicates EditorTabBar where options are located.
    ///     - colorKey: String representation of selected vignette option key.
    func barEditor(_ barEditor: EditorTabBar, didSelectVignetteOption vignetteKey: String)
    
    /// Tells about: EditorTabBar selected Blur editing option.
    /// - Parameters:
    ///     - barEditor: Indicates EditorTabBar where options are located.
    ///     - colorKey: String representation of selected blur option key.
    func barEditor(_ barEditor: EditorTabBar, didSelectBlurOption blurKey: String)
    
}
