//
//  EditorTabBar+Extensions.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 03.08.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import UIKit

// - MARK: UICollectionViewDelegate & DataSure.
extension EditorTabBar: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return barOptions.options.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if barOptions.state == .effects {
            guard let cell = tabBarCollection.dequeueReusableCell(withReuseIdentifier: effectsCell, for: indexPath) as? EffectCollectionCell else { return UICollectionViewCell() }
            cell.setContent(filterName: barOptions.options[indexPath.row], image: effectsImages[indexPath.row])
            
            if selectedEffectIndex == indexPath.row {
                cell.selectIfNeeded()
            } else {
                cell.unselectIfNeeded()
            }
            return cell
        }
        else {
            guard let cell = tabBarCollection.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as? EditorCollectionCell
            else {  return UICollectionViewCell() }
            cell.setContent(from: barOptions.options[indexPath.row])
            switch barOptions.state {
            case .crop, .lighting, .color:
                cell.allowSelection = true
            default:
                cell.allowSelection = false
            }
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let index = selectedIndex else { return }
        if let cell = collectionView.cellForItem(at: index) as? EditorCollectionCell {
            cell.selectIfNeeded()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if barOptions.state == .effects {
            let cell = collectionView.cellForItem(at: indexPath) as? EffectCollectionCell
            let optionKey = barOptions.options[indexPath.row]
            cell?.selectIfNeeded()
            editorDelegate?.barEditor(self, didSelectEffectsOption: optionKey)
            selectedEffectIndex = indexPath.row
            collectionView.reloadData()
        }
        else {
            let cell = collectionView.cellForItem(at: indexPath) as? EditorCollectionCell
            let optionKey = barOptions.options[indexPath.row]
            reloadOptionsIfNeeded(by: optionKey)
            cell?.selectIfNeeded()
            
            switch barOptions.state {
            case .text:
                editorDelegate?.barEditor(self, didSelectTextOption: optionKey)
            case .draw:
                editorDelegate?.barEditor(self, didSelectDrawOption: optionKey)
            case .effects:
                editorDelegate?.barEditor(self, didSelectEffectsOption: optionKey)
            case .orientation:
                editorDelegate?.barEditor(self, didSelectOrientationOption: optionKey)
            case .crop:
                editorDelegate?.barEditor(self, didSelectCropOption: optionKey)
            case .lighting:
                editorDelegate?.barEditor(self, didSelectLightingOption: optionKey)
            case .color:
                editorDelegate?.barEditor(self, didSelectColorOption: optionKey)
            case .vignette:
                editorDelegate?.barEditor(self, didSelectVignetteOption: optionKey)
            case .blur:
                editorDelegate?.barEditor(self, didSelectBlurOption: optionKey)
            default:
                return
            }
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? EditorCollectionCell {
            cell.unselectIfNeeded()
        }
    }
    
}

// - MARK: UICollectionViewDelegateFlowLayout
extension EditorTabBar: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.setTabBarItemsSize(from: barOptions.options.count)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return collectionView.setTabBarItemsEdges()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}
