//
//  EffectCollectionCell.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 28.08.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import UIKit

/// Indicates Effect collectionView cell.
class EffectCollectionCell: UICollectionViewCell {
    
    // - MARK: Properties
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var filterName: UILabel!
    @IBOutlet weak var selectedView: UIImageView!
    
    // - MARK: Actions
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .clear
    }
    
    // - MARK: Actions
    
    /// Sets the cell properties form an item.
    /// - Parameters:
    ///   -  filterName: Indicates filter name.
    ///   - image: Indicates filtered thumbnail Image.
    func setContent(filterName: String, image: UIImage) {
        self.imageView.image = image
        self.filterName.text = filterName
        
    }
    /// Changes the cell imageView image to selected image.
    func selectIfNeeded() {
        selectedView.isHidden = false
    }
    /// Changes the cell imageView image to normal image.
    func unselectIfNeeded() {
        selectedView.isHidden = true
    }
    
}
