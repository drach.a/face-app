//
//  EditorCollectionCell.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 30.07.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import UIKit
/// Indicates the EditorTabBar cell.
class EditorCollectionCell: UICollectionViewCell {
    
    // - MARK: Properties
    
    /// Indicates the cell imageView representation.
    @IBOutlet weak var imageView: UIImageView!
    /// Indicates highlightedImage name. By default is nil.
    var imageName = ""
    /// Indicates the cell selection state.
    var allowSelection = false
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setProperties()
    }
    
    // - MARK: Actions
    
    /// Sets the cell properties form an item.
    /// - Parameter item: For now it indicates a string item to define the cellButton image & cellLabel text.
    func setContent(from item: String) {
        imageView.image = UIImage(named: item)?.withTintColor(.label, renderingMode: .alwaysOriginal)
        imageName = item
    }
    /// Changes the cell imageView image to selected image.
    func selectIfNeeded() {
        if allowSelection {
            imageView.image = UIImage(named: "\(imageName)+Selected")
        }
    }
    /// Changes the cell imageView image to normal image.
    func unselectIfNeeded() {
        if allowSelection {
            imageView.image = UIImage(named: imageName)?.withTintColor(.label, renderingMode: .alwaysOriginal)
        }
    }
    
    // - MARk: Private Actions
    
    private func setProperties() {
        imageView.contentMode = .scaleAspectFill
        imageView.backgroundColor = .clear
        backgroundColor = .clear
    }
    
    
}
