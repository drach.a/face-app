//
//  Cropper.swift
//  face-app
//
//  Created by wouff on 09.09.2021.
//

import UIKit

class Cropper: UIImageView {
    
    /// Indicates a touch point inside the CropView.
    var innerTouch: CGPoint?
    /// Indicates a center point of the CropView in it's superview.
    var originTracker: CGPoint = .zero
    
    /// Indicates device state.
        private let device = UIDevice.current.userInterfaceIdiom
    
    /// Indicates ratio space constraint constants of cropMask and shapeMask views from the CropperView frame sides.
    private var ratioSpace: CGFloat {
        return device == .pad ? 40 : 30
        
    }
    
    var moveAvailable = true
    
    
    let topRight = CropResizer()
    let topLeft = CropResizer()
    let bottomRight = CropResizer()
    let bottomLeft = CropResizer()
    
    // - MARK: Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        setProperties()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    convenience init() {
        self.init(frame: CGRect.zero)
    }
    
    /// Sets the CropView and it's sub-views properties.
    private func setProperties() {
        contentMode = .scaleAspectFit
        backgroundColor = .clear
        isUserInteractionEnabled = true
        isOpaque = true
        translatesAutoresizingMaskIntoConstraints = true
    }
    
    
    func addResizers() {
        topRight.addTo(self, with: ratioSpace)
        topLeft.addTo(self, with: ratioSpace)
        bottomRight.addTo(self, with: ratioSpace)
        bottomLeft.addTo(self, with: ratioSpace)
        setRezizersConstraints()
        
    }
    
   private func setRezizersConstraints() {
        topRight.setConstraints(in: self, on: .topRight, with: ratioSpace)
        topLeft.setConstraints(in: self, on: .topLeft, with: ratioSpace)
        bottomRight.setConstraints(in: self, on: .bottomRight, with: ratioSpace)
        bottomLeft.setConstraints(in: self, on: .bottomLeft, with: ratioSpace)
    }
    
    
    
    
}

// - MARK: CropView Touches
extension Cropper {
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        if moveAvailable {
        guard let touch = touches.first else { return }
        innerTouch = touch.location(in: self)
        }
    }

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesMoved(touches, with: event)
        let touch = touches.first
        if moveAvailable {
        guard let superView = superview,
              let location = touch?.location(in: superview),
              let innerTouch = self.innerTouch
        else { return }

        let origin = CGPoint(x: location.x - innerTouch.x, y: location.y - innerTouch.y)
        if canMove(to: origin, in: superView) {
            frame.origin = origin
            originTracker = origin
        }
    }
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        innerTouch = nil
    }

    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
        innerTouch = nil
    }

    // - MARK: Touches Helpers

    /// Indicates a function, to determine if UIView can be moved with a touch.
    /// - Parameters:
    ///   - point: - Indicates a CGPoint in a superView where the view moves to.
    ///   - superView: - Indicates a superView of the movving view.
    /// - Returns: - A boolean value indicating the moving state.
    private func canMove(to point: CGPoint, in superView: UIView) -> Bool {
        var indicator = false
        if(point.x >= 0 && point.y >= 0) {
            if(point.x + bounds.size.width <= superView.bounds.size.width && point.y + bounds.size.height <= superView.bounds.size.height)
            {
                indicator = true
            }
        }
        return indicator
    }

}
