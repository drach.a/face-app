//
//  EditorPhotoCropper.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 27.08.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import UIKit

/// Indicates edited photo crop view.
class EditorPhotoCropper: UIImageView {
    
    // - MARK: Structs
    
    /// Indicates cropper frame sate.
    enum CropAspect: Equatable, CaseIterable {
        /// Resizable cropper frame with custom image.
        case custom
        /// Return just original image.
        case original
        /// Image aspect 1:1 crop frame.
        case square
        /// Image aspect 4:3 crop frame where 4 is width 3 is height.
        case four_three
        /// Image aspect 6:4 crop frame where 6 is width 4 is height.
        case six_four
        /// Image aspect 7:5 crop frame where 7 is width 5 is height.
        case seven_five
        /// Image aspect 10:8 crop frame where 10 is width 8 is height.
        case ten_eight
        /// Image aspect 16:9 crop frame where 16 is width 9 is height.
        case sixteen_nine
        /// To return nil
        case none
    }
    
    // - MARK: Properties
    let device = UIDevice.current.userInterfaceIdiom
    
    
    var cropAspect: CropAspect = .none

    /// Indicates UiImage for cropping.
    let cropper = Cropper()
   
    // - MARK: Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setProperties()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    convenience init() {
        self.init(frame: CGRect.zero)
    }
    
    // - MARK: Actions
    
    /// addd when crop option is selected.
        func addTo(view: UIView, with imageFrame: CGRect) {
            let x = view.frame.width / 2
            let y = view.frame.height / 2
            frame = CGRect(x: x - (imageFrame.width / 2), y: y - (imageFrame.height / 2), width: imageFrame.width, height: imageFrame.height)
            /// create frame
            view.addSubview(self)
            
        }
        
        /// Makes shapshot from cropper.
        func crop() -> UIImage {
            cropper.backgroundColor = .black
            mask = cropper
            let imageRenderer = UIGraphicsImageRenderer(bounds: mask!.frame)
            let image = imageRenderer.image { imageContext in
                layer.render(in: imageContext.cgContext)
            }
            mask = nil
            removeFromSuperview()
            return image
        }
        
        // - MARK: Private Actions
        
        private func setProperties() {
            isUserInteractionEnabled = true
            clipsToBounds = true
            translatesAutoresizingMaskIntoConstraints = true
            backgroundColor = .clear
            cropper.isUserInteractionEnabled = true
            contentMode = .scaleAspectFit
            cropper.backgroundColor = .clear
            cropper.translatesAutoresizingMaskIntoConstraints = true
        }
        
        
        /// Sets the cropper frame according to cropping aspect.
        func setCropper(with aspect: CropAspect) {
            cropAspect = aspect
            switch aspect {
            
            case .custom:
                
                if frame.width > frame.height {
                cropperFrame(width: frame.height, height: frame.height / 2)
                }
                else { cropperFrame(width: frame.width / 2, height: frame.width) }
                cropper.addResizers()
                
            case .original:
                cropper.removeFromSuperview()
                
            case .square:
                if frame.width > frame.height {
                    cropperFrame(width: frame.height, height: frame.height)
                }
                else { cropperFrame(width: frame.width, height: frame.width) }
                
            case .four_three:
                    let height = frame.width - (frame.width / 4)
                    cropperFrame(width: frame.width, height: height)
                
            case .six_four:
                let height = frame.width - (frame.width / 3)
                cropperFrame(width: frame.width, height: height)
            case .seven_five:
                let height = frame.width - (frame.size.width / 5)
                cropperFrame(width: frame.width, height: height)
            case .ten_eight:
                let height = frame.width - (frame.size.width / 8)
                cropperFrame(width: frame.width, height: height)
            case .sixteen_nine:
                let height = frame.width - (frame.width / 2 - frame.width / 16)
                cropperFrame(width: frame.width, height: height)
            case .none:
                return
            }
            
        }
        
        
        // sets cropper
        func cropperFrame(width: CGFloat, height: CGFloat) {
            cropper.layer.borderColor = UIColor.white.cgColor
            cropper.layer.borderWidth = 5
            
            let x = (frame.width / 2) - (width / 2)
            let y = (frame.height / 2) - (height / 2)
            
            cropper.frame = CGRect(x: x, y: y, width: width, height: height)
            addSubview(cropper)
            
        }
    
}
