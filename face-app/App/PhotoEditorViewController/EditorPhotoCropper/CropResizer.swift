//
//  CropResizer.swift
//  face-app
//
//  Created by wouff on 09.09.2021.
//

import UIKit

class CropResizer: UIImageView {
    
    
    enum Side {
        case topRight
        case topLeft
        case bottomRight
        case bottomLeft
        case none
    }
    
    /// Indicates a touch point inside the CropView.
    var innerTouch: CGPoint?
    
    var side: Side = .none
    
    var currentEdge: CGFloat {
        switch side {
        case .topRight, .bottomRight:
            return frame.height
        case .topLeft, .bottomLeft:
            return frame.width
        default:
            return frame.width
        }
    }
    
    // - MARK: Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        setProperties()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    convenience init() {
        self.init(frame: CGRect.zero)
    }
    
    
    /// Adds to container view
    func addTo(_ view: UIView, with ratioSize: CGFloat) {
        frame = CGRect(x: view.center.x, y: view.center.y , width: ratioSize, height: ratioSize)
        view.insertSubview(self, at: view.subviews.endIndex)
    }
    
    // constraints pin to imageview image.
    func setConstraints(in view: UIView, on side: Side, with ratioSize: CGFloat) {
        self.side = side
        
        switch side {
        case .topRight:
            topAnchor.constraint(equalTo: view.topAnchor, constant: -ratioSize / 3).isActive = true
            trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: ratioSize / 3).isActive = true
        case .topLeft:
            topAnchor.constraint(equalTo: view.topAnchor, constant: -ratioSize / 3).isActive = true
            leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: -ratioSize / 3).isActive = true
        case .bottomRight:
            bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: ratioSize / 3).isActive = true
            trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: ratioSize / 3).isActive = true
        case .bottomLeft:
            bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: ratioSize / 3).isActive = true
            leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: -ratioSize / 3).isActive = true
        default:
            return
        }
        
        widthAnchor.constraint(equalToConstant: ratioSize).isActive = true
        heightAnchor.constraint(equalToConstant: ratioSize).isActive = true
        layer.cornerRadius = frame.width / 2
        layer.masksToBounds = true
    }
    
    
    /// Sets the CropView and it's sub-views properties.
    private func setProperties() {
        contentMode = .scaleAspectFit
        backgroundColor = .white
        layer.borderWidth = 2
        layer.borderColor = UIColor.white.cgColor
        isUserInteractionEnabled = true
        isOpaque = true
        translatesAutoresizingMaskIntoConstraints = false
    }
    
    
    
    
    
    
}

extension CropResizer {
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        guard let touch = touches.first,
              let superView = superview as? Cropper
              else { return }
            superView.moveAvailable = false
            innerTouch = touch.location(in: self)
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesMoved(touches, with: event)
        guard let touch = touches.first,
              let cropper = superview as? Cropper else { return }
        let currentPoint = touch.location(in: cropper)
        let previous = touch.previousLocation(in: cropper)
        
        let originX = cropper.frame.origin.x
        let originY = cropper.frame.origin.y
        let width = cropper.frame.size.width
        let height = cropper.frame.size.height
        
        let deltaWidth = currentPoint.x - previous.x
        let deltaHeight = currentPoint.y - previous.y
        
        switch side {
        case .topRight:
            if canMove(to: CGPoint(x: originX, y: originY), in: cropper.superview!) {
                cropper.frame = CGRect(x: originX + deltaWidth, y: originY + deltaHeight, width: width - deltaWidth, height: height - deltaHeight)
            }
        case .topLeft:
            if canMove(to: CGPoint(x: originX, y: originY), in: cropper.superview!) {
            cropper.frame = CGRect(x: originX + deltaWidth, y: originY + deltaHeight, width: width - deltaWidth, height: height - deltaHeight)
            }
        case .bottomRight:
            if canMove(to: CGPoint(x: originX, y: originY), in: cropper.superview!) {
            cropper.frame = CGRect(x: originX - deltaWidth, y: originY - deltaHeight, width: width + deltaWidth, height: height + deltaHeight)
            }
        case .bottomLeft:
            if canMove(to: CGPoint(x: originX, y: originY), in: cropper.superview!) {
            cropper.frame = CGRect(x: originX + deltaWidth, y: originY - deltaHeight, width: width - deltaWidth, height: height + deltaHeight)
            }
        case .none:
            return
        
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        guard let superView = superview as? Cropper else { return }
        superView.moveAvailable = true
        innerTouch = nil
        
    }
    
    
    private func canMove(to point: CGPoint, in superView: UIView) -> Bool {
        var indicator = false
        if(point.x >= 10 && point.y >= 10) {
            if(point.x + bounds.size.width <= superView.bounds.size.width - 10 && point.y + bounds.size.height <= superView.bounds.size.height - 10)
            {
                indicator = true
            }
        }
        return indicator
    }
    
   
    
}
