//
//  EditorControllerDelegate.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 17.08.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

/// Delegates the PhotoEditorViewController actions.
protocol EditorControllerDelegate: AnyObject {
    
    /// Tells about: the PhotoEditorViewController is disappeard.
    /// - Parameters:
    ///     - editorController: Indicates  editorView controller.
    ///     - finished: Indicates bool value of finishing photo editing.
    func editorController(_ editorController: PhotoEditorViewController, didFinishEditing finished: Bool)
    
    /// Tells about: the PhotoEditorViewController is disappeard.
    /// - Parameters:
    ///     - editorController: Indicates  editorView controller.
    ///     - didLoad: Indicates bool value of  secsesffuly loaded controllers view.
    func editorController(_ editorController: PhotoEditorViewController, didLoadView: Bool)

}
