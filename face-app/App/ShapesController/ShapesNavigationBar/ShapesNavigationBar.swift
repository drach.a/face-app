//
//  ShapesNavigationBar.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 05.08.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import UIKit

/// Indicates ShapesViewController navigationBar.
class ShapesNavigationBar: UIView {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    // - MARK: Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setProperties()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setProperties()
    }
    
    // - MARK: Private Actions
    
    @IBAction private func cancelAction(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name("dismiss"), object: nil)
    }
    
    private func setProperties() {
        self.loadFromNib(name: "ShapesNavigationBar")
        backgroundColor = .clear
    }
    
}
