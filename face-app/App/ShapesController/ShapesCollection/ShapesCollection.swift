//
//  ShapesCollection.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 05.08.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import UIKit

/// Indicates view for face shapes collection.
class ShapesCollection: UIView {
    
    // - MARK: Properties
    
    /// Indicates the collection of shapes.
    @IBOutlet weak var shapesCollection: UICollectionView!
    /// Indicates the TabBar collection cell id.
    let cellId = "ShapeCell"
    /// ShapeDelegate.
    weak var delegate: ShapesCollectionDelegate?
    /// Indicates the shapesCollection content.
    let shapes: [String] = ["face1", "face2", "face3", "face4", "face5", "face6"]
    
    // - MARK: Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setProperties()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setProperties()
    }
    
    // - MARK: Actions
    
    func closeCollection() {
        NotificationCenter.default.post(name: Notification.Name("dismiss"), object: nil)
    }
    
    // - MARK: Private Actions
    
    /// Sets properties of the StartTabBar.
    private func setProperties() {
        self.loadFromNib(name: "ShapesCollection")
        shapesCollection.backgroundColor = .clear
        setDelegates()
    }
    
    /// Sets needed delegates.
    private func setDelegates() {
        shapesCollection.register(UINib(nibName: "ShapeCollectionCell", bundle: nil), forCellWithReuseIdentifier: cellId)
        shapesCollection?.dataSource = self
        shapesCollection?.delegate = self
    }
    
    
}
