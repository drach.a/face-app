//
//  ShapesCollectionDelegate.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 06.08.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import UIKit

/// Delegates the ShapesCollection object  actions.
protocol ShapesCollectionDelegate: AnyObject {
    
    /// Tells about: the ShapesCollection picked needed shape..
    /// - Parameters:
    ///     - shapesCollection: Indicates ShapesCollection where shapes are located..
    ///     - shape: Indicates the String representation of picked shape image name.
    func shapesCollection(_ shapesCollection: ShapesCollection, didPickShape shape: String)

}
