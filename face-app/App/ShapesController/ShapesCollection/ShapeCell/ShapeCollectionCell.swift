//
//  ShapeCollectionCell.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 05.08.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import UIKit

/// Indicates the ShapesCollection cell.
class ShapeCollectionCell: UICollectionViewCell {
    
    // - MARK: Properties
    
    /// Indicates the shape ImageView.
    @IBOutlet weak var imageView: UIImageView!
    /// Indicates image name.
    var imageName = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setProperties()
    }
    
    // - MARK: Actions
    
    /// Sets the cell properties form an item.
    /// - Parameter item: For now it indicates a string item to define the cellButton image & cellLabel text.
    func setContent(from item: String) {
        imageView.image = UIImage(named: item)?.withTintColor(.label, renderingMode: .alwaysOriginal)
        imageName = item
    }
    /// Changes the cell imageView image to selected image.
    func selectIfNeeded() {
        let selectionColor: UIColor = .systemTeal
        let selectedImage = UIImage(named: imageName)?.withTintColor(selectionColor, renderingMode: .alwaysOriginal)
        imageView.image = selectedImage
    }
    /// Changes the cell imageView image to normal image.
    func unselectIfNeeded() {
        imageView.image = UIImage(named: imageName)?.withTintColor(.label, renderingMode: .alwaysOriginal)
    }
    
    // - MARK: Private Actions
    
    /// Sets the cell properties
    private func setProperties() {
        imageView.contentMode = .scaleAspectFit
        imageView.backgroundColor = .clear
        backgroundColor = .clear
    }
}
