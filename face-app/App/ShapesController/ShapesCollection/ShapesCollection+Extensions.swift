//
//  ShapesCollection+Extensions.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 05.08.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import Foundation
import UIKit

// - MARK: UICollectionViewDelegate & DataSure.
extension ShapesCollection: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  shapes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as? ShapeCollectionCell
        else { fatalError("Cannot dequeue cell for indexPath: \(indexPath)!") }
        cell.setContent(from: shapes[indexPath.row])
        return cell
    }
    
    // When selected.
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? ShapeCollectionCell else { return }
        cell.selectIfNeeded()
        let shape = shapes[indexPath.row]
        delegate?.shapesCollection(self, didPickShape: shape)
        closeCollection()
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? ShapeCollectionCell else { return }
        cell.unselectIfNeeded()
    }
    
}

// - MARK: UICollectionViewDelegateFlowLayout
extension ShapesCollection: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        /// Indicates each item size to return.
        var size: CGSize = CGSize()
        
        // Calculating width:
        let width = collectionView.frame.width - (collectionView.contentInset.left + collectionView.contentInset.right)
        
        size.width = width / 3
        size.height = width / 3
        
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return  UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}

