//
//  ShapesViewController.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 05.08.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import UIKit

/// Contains collectionView with shapes.
class ShapesViewController: UIViewController {
    
    // - MARK: Properties
    
    @IBOutlet weak var shapesNavBar: ShapesNavigationBar!
    @IBOutlet weak var shapesCollection: ShapesCollection!
    
    // - MARK: Init
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setObservers()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        shapesNavBar.setShadows()
    }
    
    // - MARK: Private Actions
    
    private func setObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(cancelShapes), name: Notification.Name("dismiss"), object: nil)
    }
    
    @objc private func cancelShapes() {
        dismiss(animated: true) {
        }
    }
    
}

