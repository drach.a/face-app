//
//  UIView+Extensions.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 01.08.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import UIKit

/// - MARK: UIImageView Extensions
extension UIImageView {
    
    /// Returns a UIImage rendered from a UIImageView mask, otherwise rendered from it's bounds.
    func imageFromMask() -> UIImage {
        let clippingMask = mask?.frame ?? bounds
        let imageRenderer = UIGraphicsImageRenderer(bounds: clippingMask)
        return imageRenderer.image { imageContext in
            layer.render(in: imageContext.cgContext)
        }
    }
    
    
    func imageFrame() -> CGRect {
        let imgSize = image?.size ?? frame.size
           
            let scaleW = frame.width / imgSize.width
            let scaleH = frame.height / imgSize.height
            let aspect = min(scaleW, scaleH)
            
           // Center image
            let width = imgSize.width * aspect
            let height = imgSize.height * aspect
           
            let x = frame.origin.x + (frame.width + width / 2)
            let y = frame.origin.y + (frame.height + height / 2)
            
            return CGRect(x: x, y: y, width: width, height: height)
       }
    
    
    /// Takes out rendering UIImage from UIIImageView frame.
    func takeoutImage() -> UIImage? {
        guard let imageSize = image?.size else { return nil }
        
        // Calculating image rect inside the UIImageView.
        let scaleWidth = frame.width / imageSize.width
        let scaleHeight = frame.height / imageSize.height
        let scaleAspect = min(scaleWidth, scaleHeight)
        
        let width = imageSize.width * scaleAspect
        let height = imageSize.height * scaleAspect
        
        let x = frame.origin.x + (frame.width + width / 2)
        let y = frame.origin.y + (frame.height + height / 2)
        
        let imageRect = CGRect(x: x, y: y, width: width, height: height)
        
        let view = UIView()
        view.backgroundColor = .black
        view.frame = imageRect
        view.center = CGPoint(x: frame.size.width / 2, y: frame.size.height / 2)
        addSubview(view)
        mask = view
        
        // Taking out the image from the UIImageView context.
        let renderer = UIGraphicsImageRenderer(bounds: mask?.frame ?? bounds)
        let renderedImage = renderer.image { imageContext in
            layer.render(in: imageContext.cgContext)
        }
        mask = nil
        return renderedImage
    }
    
    
}

// - MARK: UIView Extension
extension UIView {
    
    
    /// Renders UIImage from UiView bounds
    func uiImage() -> UIImage {
        let renderer = UIGraphicsImageRenderer(bounds: bounds)
        return renderer.image { context in
            layer.render(in: context.cgContext)
        }
    }
    
    
    
    // Sets UIView bounds shadows for good look, needs be called in ViewDidLayout subView.
    func setShadows() {
        layer.shadowColor = UIColor.systemTeal.cgColor
        layer.shadowOpacity = 0.8
        layer.shadowOffset = .zero
        layer.shadowRadius = 5
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
    }
    
    /// Helper to get pre transformed UIView frame after rotation or scaling.
    var originalFrame: CGRect {
        let currentTransform = transform
        transform = .identity
        let originalFrame = frame
        transform = currentTransform
        return originalFrame
    }
    
    /// Loads the UIView from xib file.
    /// - Parameters:
    ///     - name: Indicates correspondin UiView xib file like: MyCustomView.xib .
    func loadFromNib(name fileName: String) {
        guard let view = Bundle.main.loadNibNamed(fileName, owner: self, options: nil)?.first as? UIView else { return }
        view.frame = bounds
        addSubview(view)
    }
    
    /// returns UIColor from CGPoint..
    /// - Parameters:
    ///     - point: Indicates color CGPoint..
    func colorFromPoint(_ point: CGPoint) -> UIColor {
        let space = CGColorSpaceCreateDeviceRGB()
        let info = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        var data: [UInt8] = [0, 0, 0, 0]
        guard let context = CGContext(data: &data, width: 1, height: 1, bitsPerComponent: 8, bytesPerRow: 4, space: space, bitmapInfo: info.rawValue) else { return .clear}
        context.translateBy(x: -point.x, y: -point.y)
        layer.render(in: context)
        
        let red = CGFloat(data[0]) / CGFloat(255.0)
        let green = CGFloat(data[1]) / CGFloat(255.0)
        let blue = CGFloat(data[2]) / CGFloat(255.0)
        let alpha = CGFloat(data[3]) / CGFloat(255.0)
        
        return UIColor(red: red, green: green, blue: blue, alpha: alpha)
    }
    
}

// - MARK: UICollectionView Extension
extension UICollectionView {
    
    /// Calculates items size for UICollectionView:
    /// - Parameters:
    ///     - itemsCount: - Indicates number of items in an array for the CollectionView.
    /// - Returns: - CGSize for each item.
    func setTabBarItemsSize(from itemsCount: Int) -> CGSize {
        /// Indicates each item size to return.
        var size: CGSize = CGSize()
        let device = UIDevice.current.userInterfaceIdiom
        
        // Calculating height:
        let height = frame.height - (contentInset.top + contentInset.bottom)
        size.height = height
        
        // Calculating width:
        let width = frame.width - (contentInset.left + contentInset.right)
        
        // For the iPhone is need to display 5 items, if them are 5 or more than 5, all other will be available by scrolling.
        if device == .phone &&  itemsCount >= 5 {
            size.width = width / 5
        }
        // All other cases including iPad.
        else if device == .pad && itemsCount >= 9 {
            size.width = width / 9
        }
        else {
            size.width = width / CGFloat(itemsCount)
        }
        return size
    }
    
    /// Returns edges insets for UICollectionView.
    func setTabBarItemsEdges() -> UIEdgeInsets {
        /// Device state.
        let device = UIDevice.current.userInterfaceIdiom
        /// Indicates top cells inset from top of the CollectionView.
        let top: CGFloat = device == .phone ? 5 : 10
        /// It's size of safeArea, what was deleted in  storiboard or xib file and extended here for a convenience.
        let bottom: CGFloat = 30
        /// Indicates right and left insets: Important it should be zero for equel items distribution.
        let rightLeft: CGFloat = 0
        return UIEdgeInsets(top: top, left: rightLeft, bottom: bottom, right: rightLeft)
    }
    
}
