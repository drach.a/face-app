//
//  UIImage+Extensions.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 01.08.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import UIKit

// - MARK: UIImage
extension UIImage {
    
    /// Resizes images if resolution is to large to work with.
    /// - Parameters:
    ///   - resolution: - Inicates maximum allowed resolution.
    /// - Returns: - A new UIImage with remade resolution.
    func resized(with resolution: Float) -> UIImage {
        var currentHeight = Float(size.height)
        var currentWidth = Float(size.width)
        let maxHeight: Float = resolution
        let maxWidth: Float = resolution
        var imageRatio: Float = currentWidth / currentHeight
        let maxRatio: Float = maxWidth / maxHeight
        let compression: CGFloat = 1.0
        
        if currentHeight > maxHeight || currentWidth > maxWidth {
            if imageRatio < maxRatio {
                imageRatio = maxHeight / currentHeight
                currentWidth = imageRatio * currentWidth
                currentHeight = maxHeight
            }
            else if imageRatio > maxRatio {
                imageRatio = maxWidth / currentWidth
                currentHeight = imageRatio * currentHeight
                currentWidth = maxWidth
            }
            else {
                currentHeight = maxHeight
                currentWidth = maxWidth
            }
        }
        let rect = CGRect(x: 0, y: 0, width: CGFloat(currentWidth), height: CGFloat(currentHeight))
        UIGraphicsBeginImageContext(rect.size)
        draw(in: rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        guard let imageData = image?.jpegData(compressionQuality: compression) else { return self }
        UIGraphicsEndImageContext()
        return UIImage(data: imageData) ?? self
    }
    
    /// Rotates UIImage.
    /// - Parameters:
    ///   - degrees: - Inicates rotation UIImage angle.
    ///   - flip: Indicates the image flipping state.
    /// - Returns: - A new transformed UIImage.
    func imageRotatedByDegrees(degrees: CGFloat, flip: Bool) -> UIImage {
        let degreesToRadians: (CGFloat) -> CGFloat = {
            return $0 / 180.0 * CGFloat.pi
        }
        
        let rotatedViewBox = UIView(frame: CGRect(origin: .zero, size: size))
        let transform = CGAffineTransform(rotationAngle: degreesToRadians(degrees))
        rotatedViewBox.transform = transform
        let rotatedSize = rotatedViewBox.frame.size
        
        UIGraphicsBeginImageContext(rotatedSize)
        let bitmap = UIGraphicsGetCurrentContext()
        bitmap?.translateBy(x: rotatedSize.width / 2.0, y: rotatedSize.height / 2.0)
        bitmap?.rotate(by: degreesToRadians(degrees))
        var yFlip: CGFloat
        
        if(flip){
            yFlip = CGFloat(-1.0)
        } else {
            yFlip = CGFloat(1.0)
        }
        
        bitmap?.scaleBy(x: yFlip, y: -1.0)
        let rect = CGRect(x: -size.width / 2, y: -size.height / 2, width: size.width, height: size.height)
        bitmap?.draw(cgImage!, in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    
    func crop(rect: CGRect) -> UIImage {
            var rect = rect
            rect.origin.x*=self.scale
            rect.origin.y*=self.scale
            rect.size.width*=self.scale
            rect.size.height*=self.scale

            let imageRef = self.cgImage!.cropping(to: rect)
            let image = UIImage(cgImage: imageRef!, scale: self.scale, orientation: self.imageOrientation)
            return image
        }
    
    func crop(to:CGSize) -> UIImage {
            guard let cgimage = self.cgImage else { return self }

            let contextImage: UIImage = UIImage(cgImage: cgimage)

            let contextSize: CGSize = contextImage.size

            //Set to square
            var posX: CGFloat = 0.0
            var posY: CGFloat = 0.0
            let cropAspect: CGFloat = to.width / to.height

            var cropWidth: CGFloat = to.width
            var cropHeight: CGFloat = to.height

            if to.width > to.height { //Landscape
                cropWidth = contextSize.width
                cropHeight = contextSize.width / cropAspect
                posY = (contextSize.height - cropHeight) / 2
            } else if to.width < to.height { //Portrait
                cropHeight = contextSize.height
                cropWidth = contextSize.height * cropAspect
                posX = (contextSize.width - cropWidth) / 2
            } else { //Square
                if contextSize.width >= contextSize.height { //Square on landscape (or square)
                    cropHeight = contextSize.height
                    cropWidth = contextSize.height * cropAspect
                    posX = (contextSize.width - cropWidth) / 2
                }else{ //Square on portrait
                    cropWidth = contextSize.width
                    cropHeight = contextSize.width / cropAspect
                    posY = (contextSize.height - cropHeight) / 2
                }
            }

            let rect: CGRect = CGRect(x: posX, y: posY, width: cropWidth, height: cropHeight)
            // Create bitmap image from context using the rect
            let imageRef: CGImage = contextImage.cgImage!.cropping(to: rect)!

            // Create a new image based on the imageRef and rotate back to the original orientation
            let cropped: UIImage = UIImage(cgImage: imageRef, scale: self.scale, orientation: self.imageOrientation)

            UIGraphicsBeginImageContextWithOptions(to, true, self.scale)
            cropped.draw(in: CGRect(x: 0, y: 0, width: to.width, height: to.height))
            let resized = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()

            return resized!
        }
    
}
