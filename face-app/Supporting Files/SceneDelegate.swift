//
//  SceneDelegate.swift
//  face-app
//
//  Created by Alexandr Drach for the Wouff on 27.07.2021.
//  Copyright © 2021. Alexandr Drach. All rights reserved.

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    var window: UIWindow?
    static weak var shared: SceneDelegate?
    var sceneCopy : UIWindowScene?
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        
        guard let winScene = (scene as? UIWindowScene) else { return }
        
        Self.shared = self
        self.sceneCopy = winScene
        
        SubManager.shared.requestProducts()
        SubManager.shared.completeTransactions()
        SubManager.shared.validationSubscription { (result) in
            if result {
                self.pushApp(scene: winScene)
            }else {
                self.push(scene: winScene)
            }
        }
    }
    
    func push(scene: UIWindowScene) {
        
        window = UIWindow(windowScene: scene)
        let nc = UINavigationController()
        let vc = SubscriptionController()
        nc.viewControllers = [vc]
        window?.rootViewController = nc
        window?.makeKeyAndVisible()
    }
    
    func pushApp(scene : UIWindowScene) {
        window = UIWindow(windowScene: scene)
        window?.rootViewController = StartViewController()
        window?.makeKeyAndVisible()
    }
    
    func sceneDidDisconnect(_ scene: UIScene) {
        
    }
    
    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }
    
    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }
    
    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }
    
    func sceneDidEnterBackground(_ scene: UIScene) {
        
    }
    
    
}

