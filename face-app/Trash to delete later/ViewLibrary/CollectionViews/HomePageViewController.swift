//
//  HomePageViewController.swift
//  face-app
//
//  Created by michael on 24.06.2021.
//

import UIKit


// 

class HomePageViewController: UIViewController, UICollectionViewDelegateFlowLayout {

    // - MARK: Properties
    
    let buttonPadding:CGFloat = 10
    var xOffset:CGFloat = 10
    var test = ["Text", "Hi"]
    var image = [UIImage(named: "text"), UIImage(named: "draw"), UIImage(named: "effects"), UIImage(named: "orientation"), UIImage(named: "crop"), UIImage(named: "lighting"), UIImage(named: "color"), UIImage(named: "vignette"), UIImage(named: "blur")]
    var cellId = "Cell"
    var image1 = ["text", "effects" ]
    //weak var slider: Slider?
//    let tabItems: [EditPhotosOptions] = [.text]
    
    
    @IBOutlet weak var tool: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let layout = tool.collectionViewLayout as? UICollectionViewFlowLayout else { return }
        layout.sectionInset = UIEdgeInsets(top: 20, left: 17, bottom: 50, right: 17)
        layout.itemSize = CGSize(width: 40, height: 40)
              
        tool?.register(TabBarCollectionViewCell.self, forCellWithReuseIdentifier: cellId)
        tool?.backgroundColor = UIColor.white
        tool?.dataSource = self
        tool?.delegate = self
        self.view.addSubview(tool)
        
       // setupSlider()
        
//        self.view = view
    }
    
    
    // - MARK: Private Actions
    
//    private func setupSlider() {
//        let slider = Slider(frame: .zero)
//        slider.translatesAutoresizingMaskIntoConstraints = false
//        self.view.addSubview(slider)
//        slider.isHidden = true
//        self.slider = slider
//    }
}



// - MARK: UICollectionViewDataSource
extension HomePageViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return image.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! TabBarCollectionViewCell
        cell.backgroundColor = #colorLiteral(red: 0.8666267991, green: 0.8666279912, blue: 0.8794677854, alpha: 1)
        cell.layer.cornerRadius = 20
        cell.imageView.image = image[indexPath.item]
//        cell.label.text = image1[indexPath.row]

        return cell
    }
}

// - MARK: UICollectionViewDelegate
extension HomePageViewController: UICollectionViewDelegate {
 
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            
        }
       print("tapped on \(indexPath.row)")
    }
}
